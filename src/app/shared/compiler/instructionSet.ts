import { Instruction } from "./instruction";
export class InstructionSet {

    private _microController: string;
    private _instructionsSet = new Map<string, Instruction>();

    public constructor (microController: string) {
        this._microController = microController;

        /* BYTE-ORIENTED FILE REGISTER OPERATIONS */
        this.insertInstruction("ADDWF", ["f", "d"], "Add W and f", 1, "0b000111", "000111dfffffff");
        this.insertInstruction("ANDWF", ["f", "d"], "AND W with f", 1, "0b000101", "000101dfffffff");
        this.insertInstruction("CLRF", ["f"], "Clear f", 1, "0b000001", "0000011fffffff");
        this.insertInstruction("CLRW", [], "Clear W", 1, "0b000001", "0000010xxxxxxx");
        this.insertInstruction("COMF", ["f", "d"], "Complement f", 1, "0b001001", "001001dfffffff");
        this.insertInstruction("DECF", ["f", "d"], "Decrement f", 1, "0b000011", "000011dfffffff");
        this.insertInstruction("DECFSZ", ["f", "d"], "Decrement f, Skip if 0", 2, "0b001011", "001011dfffffff");
        this.insertInstruction("INCF", ["f", "d"], "Increment f", 1, "0b001010", "001010dfffffff");
        this.insertInstruction("INCFSZ", ["f", "d"], "Increment f, Skip if 0", 2, "0b001111", "001111dfffffff");
        this.insertInstruction("IORWF", ["f", "d"], "Inclusive OR W with f", 1, "0b000100", "000100dfffffff");
        this.insertInstruction("MOVF", ["f", "d"], "Move f", 1, "0b001000", "001000dfffffff");
        this.insertInstruction("MOVWF", ["f", "d"], "Move W to f", 1, "0b000000", "0000001fffffff");
        this.insertInstruction("NOP", [], "No Operation", 1, "0b000000", "0000000xx00000");
        this.insertInstruction("RLF", ["f", "d"], "Rotate Left f through Carry", 1, "0b001101", "001101dfffffff");
        this.insertInstruction("RRF", ["f", "d"], "Rotate Right f through Carry", 1, "0b001100", "001100dfffffff");
        this.insertInstruction("SUBWF", ["f", "d"], "Subtract W from f", 1, "0b000010", "000010dfffffff");
        this.insertInstruction("SWAPF", ["f", "d"], "Swap nibbles in f", 1, "0b001110", "001110dfffffff");
        this.insertInstruction("XORWF", ["f", "d"], "Exclusive OR W with f", 1, "0b000110", "000110dfffffff");

        /* BIT-ORIENTED FILE REGISTER OPERATIONS */
        this.insertInstruction("BCF", ["f", "b"], "Bit Clear f", 1, "0b010000", "0100bbbfffffff");
        this.insertInstruction("BSF", ["f", "b"], "Bit Set f", 1, "0b010100", "0101bbbfffffff");
        this.insertInstruction("BTFSC", ["f", "b"], "Bit Test f, Skip is Clear", 1, "0b011000", "0110bbbfffffff");
        this.insertInstruction("BTFSS", ["f", "b"], "Bit Test f, Skip is Set", 1, "0b011100", "0111bbbfffffff");

        /* LITERAL AND CONTROL OPERATIONS */
        this.insertInstruction("ADDLW", ["k"], "Add Literal and W", 1, "0b111110", "111110kkkkkkkk");
        this.insertInstruction("ANDLW", ["k"], "AND Literal with W", 1, "0b11111001", "111001kkkkkkkk");
        this.insertInstruction("CALL", ["k"], "Call subroutine", 2, "0b100", "100kkkkkkkkkkk");
        this.insertInstruction("CLRWDT", [], "Clear Whatchdog Timer W", 1, "0b111110", "00000001100100");
        this.insertInstruction("GOTO", ["k"], "Go to address", 2, "0b101", "101kkkkkkkkkkk");
        this.insertInstruction("IORLW", ["k"], "Inclusive OR literal with W", 1, "0b111000", "111000kkkkkkkk");
        this.insertInstruction("MOVLW", ["k"], "Move literal to W", 1, "0b110000", "110000kkkkkkkk");
        this.insertInstruction("RETFIE", [], "Return from interrupt", 1, "0b000000", "00000000001001");
        this.insertInstruction("RETLW", ["k"], "Return with literal in W", 1, "0b110100", "110100kkkkkkkk");
        this.insertInstruction("RETURN", [], "Return from Subroutine", 1, "0b000000", "00000000001000");
        this.insertInstruction("SLEEP", [], "Go into standby mode", 1, "0b000000", "00000001100011");
        this.insertInstruction("SUBLW", ["k"], "Subtract W from literal", 1, "0b111100", "111100kkkkkkkk");
        this.insertInstruction("XORLW", ["k"], "Exclusive OR literal with W", 1, "0b111010", "111010kkkkkkkk");

    }

    public getInstructionsSet(): Map<string, Instruction> {
        return this._instructionsSet;
    }

    public setInstructionsSet(instructionsSet: Map<string, Instruction>) {
        this._instructionsSet = instructionsSet;
    }

    public getMicroController(): string {
        return this._microController;
    }

    public setMicroController(microController: string) {
        this._microController = microController;
    }

    // tslint:disable-next-line:max-line-length
    private insertInstruction( mnemonic: string, operands: string[], description: string, cycles: number, opCode: string, opCode14: string) {
        const insASM = new Instruction(mnemonic, operands, description, cycles, opCode, opCode14);
        this._instructionsSet.set(mnemonic, insASM);
    }

    public verifyInstruction( instruction: string): any {
      return this._instructionsSet.has(instruction);
    }

    public getInstructionDetails (nmonic: string): Instruction {
      console.log(nmonic);
      console.log(this._instructionsSet.get(nmonic));
      return this._instructionsSet.get(nmonic);
    }

}
