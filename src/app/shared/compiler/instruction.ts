import { isNullOrUndefined } from "util";

export class Instruction {
         private mnemonic: string;
         private operands: string[];
         private description: string;
         private cycles: number;
         private opCode: string;
         //  private opCode14 = ["", "", ""];
         private opCode14: string;
         private opCode14format: string;

         constructor(mnemonic: string, operands: string[], description: string, cycles: number, opCode: string, opCode14: string) {
           this.mnemonic = mnemonic;
           this.operands = operands;
           this.description = description;
           this.cycles = cycles;
           this.opCode = opCode;
           this.opCode14format = opCode14;
           this.opCode14 = opCode14;
           //  this.opCode14[0] = opCode14.slice(0, 2);
           //  this.opCode14[1] = opCode14.slice(2, 6);
           //  this.opCode14[2] = opCode14.slice(6, 14);
         }

         public getMnemonic(): string {
           return this.mnemonic;
         }

         public setMnemonic(mnemonic: string) {
           this.mnemonic = mnemonic;
         }

         public getOperands(): string[] {
           return this.operands;
         }

         public getFirstOperand(): string {
           return this.operands[0];
         }

         public getSecondOperand(): string {
           return this.operands[1];
         }

         public setOperands(operands: string[]) {
           this.operands = operands;
         }

         public getDescription(): string {
           return this.description;
         }

         public setDescription(description: string) {
           this.description = description;
         }

         public getCycles(): number {
           return this.cycles;
         }

         public setCycles(cycles: number) {
           this.cycles = cycles;
         }

         public getOpCode(): string {
           return this.opCode;
         }

         public setOpCode(opCode: string) {
           this.opCode = opCode;
         }

         public getOpCode14Format(): string {
           return this.opCode14format;
         }

         public getOpCode14(): string {
           return this.opCode14;
         }

         public setOpCode14(opCode14: string) {
           //  this.opCode14[0] = opCode14.slice(0, 2);
           //  this.opCode14[1] = opCode14.slice(2, 6);
           //  this.opCode14[2] = opCode14.slice(6, 14);
           this.opCode14 = opCode14;
         }

        public fillOpcode14() {
          const firstOperand = this.getFirstOperand();
          const secondOperand = this.getSecondOperand();

          if (!isNullOrUndefined(firstOperand)) {
            let opcode = this.opCode14format;
            if (this.opCode14format.search("x") !== -1) {
              opcode = opcode.replace(/x/gi, "0");
            }

            if (this.opCode14format.search("f") !== -1) {
              const firstFIndex = opcode.indexOf("f");
              opcode = opcode.replace("fffffff", firstOperand.slice(firstFIndex, firstOperand.length));
              if (this.opCode14format.search("b") !== -1) {
                // console.log("##### b ########{" + secondOperand + "}################");
                // console.log("##### b ########{" + secondOperand.slice(5, 8) + "}################");
                const firstBIndex = opcode.indexOf("b");
                opcode = opcode.replace("bbb", secondOperand.slice(secondOperand.length - 3, secondOperand.length));
              }
              if (this.opCode14format.search("d") !== -1) {
                // console.log("##### d ########{" + secondOperand + "}################");
                // console.log("##### d ########{" + secondOperand.slice(7, 8) + "}################");
                const firstDIndex = opcode.indexOf("d");
                opcode = opcode.replace("d", secondOperand.slice(secondOperand.length - 1, secondOperand.length));
              }

            } else if (this.opCode14format.search("k") !== -1) {
              const firstKIndex = opcode.indexOf("k");
              const opcodeParams1 = opcode.slice(0, firstKIndex);
              let opcodeParams2 = opcode.slice(firstKIndex, opcode.length);
              // let opcodeParams2 = opcode.slice(6, 14);
              opcodeParams2 = firstOperand.slice(firstOperand.length - opcodeParams2.length, firstOperand.length); // opcodeParams2.replace(opcode.substring(opcode.indexOf("k")), firstOperand); // .slice(0, 8));
              opcode = opcodeParams1 + opcodeParams2;
            }
            this.setOpCode14(opcode);
          }
        }

         public tostring(): string {
           // tslint:disable-next-line:max-line-length
           return "Instruction{" + "mnemonic: " + this.mnemonic + "\n" + ", operands: " + this.operands + "\n" + ", description: " + this.description + "\n" + ", cycles: " + this.cycles + "\n" + ", opCode: " + this.opCode + "\n" + ", opCode14: " + this.opCode14 + "}" + "\n";
         }
       }
