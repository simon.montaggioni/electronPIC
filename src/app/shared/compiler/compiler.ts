import { Instruction } from "./instruction";
import { InstructionSet } from "./instructionSet";
import { ModuleWithComponentFactories } from "@angular/core";
import { isNullOrUndefined, isUndefined, isNull } from "util";
import { forEach } from "@angular/router/src/utils/collection";
import { IncludeAsm } from "../include";
import { ToBinaryService } from "../../services/toBinary.service";
export class Compiler {

  private static counter = 1;
  private instructions = new Map<String, Instruction>();
  private instrucctionCounter: number;
  private instructionSet;
  private equivalenceTable = new Map<string, string>();
  private asmInstructionsList = ["EQU", "END", "ORG", "INCLUDE", "LIST"];
  private markCaracter = "@";
  private realCode = "";
  private testCode = "";
  private binaryCode = "";
  private error = false;
  private lineCounter;
  private toBinary: ToBinaryService;

  public outputCode: string;
  public compiledCode: string;

  public constructor( ) {
    this.instructionSet = new InstructionSet("16f84A");
    this.toBinary = new ToBinaryService();
  }

  public run(code: string) {
    this.outputCode = "";
    this.binaryCode = "";
    this.instructions.clear();
    this.equivalenceTable.clear();
    this.testCode = code.toUpperCase();
    const codeLines = this.splitCodeIntoLines(this.testCode);
    this.lineCounter = 1;
    this.instrucctionCounter = 0;

    this.searchAndSaveLabels(this.testCode);
    for (let line of codeLines) {
      this.realCode = this.realCode + "line #" + this.lineCounter + ": " + line + "\n";
      line = this.replaceWhiteSpaces(line);
      line = this.cleanComments(line);
      // console.log("line #" + this.lineCounter + ": " + line);

      if (!isNullOrUndefined(line.match( /[^@]/g))) {

        this.outputCode = this.outputCode + "line #" + this.lineCounter + ": " + line + "\n";
        if (this.hasLabel(line)) {
          this.fillEquivalenceTable(this.getLabel(line));
          line = this.removeLabel(line);
        }

        if ((this.hasAsmInstruction(line)) &&
                   (this.getAsmInstruction(line) === "EQU")) {
            this.fillEquivalenceTable(line);
        } else if ((this.hasAsmInstruction(line)) &&
                   (this.getAsmInstruction(line) === "LIST")) {
            // TODO
        } else if ((this.hasAsmInstruction(line)) &&
                   (this.getAsmInstruction(line) === "INCLUDE")) {
            // TODO include PIC register equivalence
            const includeFile = new IncludeAsm();
            const codeLinesInclude = this.splitCodeIntoLines(includeFile.getIncludeSTring());
            for (let lineInclude of codeLinesInclude) {
              lineInclude = this.replaceWhiteSpaces(lineInclude);
              lineInclude = this.cleanComments(lineInclude);
                this.fillEquivalenceTable(lineInclude);
            }
        } else if (this.hasMicrocontrollerInstruction(line)) {
            this.pushInstruction(line);
        } else {
          const patternt = /[^@]/g;
          const result = line.match(patternt);
          // console.log(" Found an ERROR at line: " + this.lineCounter + " >>> {" + result + "}");
        }
      }
      this.lineCounter++;
    }
    this.generateInstructions();
  }

  public replaceWhiteSpaces (code: string): string {
    code = code.replace(/\s|\t/gi, this.markCaracter);
    return code;
  }

  public splitCodeIntoLines (code: string): Array<string> {
    let splitCode: Array<string>;
    splitCode = code.split("\n");
    return splitCode;
  }

  public cleanComments ( line: string): string {
    const commentBeginning = line.search(";");
    if (commentBeginning === 0) {
      line = this.markCaracter;
    } else if (commentBeginning !== -1) {
      line = line.slice(0, commentBeginning);
    }
    return line;
  }

  public searchAndSaveLabels(testCode: string): boolean {
    let result = false;
    const codeLines = this.splitCodeIntoLines(testCode);
    for (let line of codeLines) {
      line = this.replaceWhiteSpaces(line);
      line = this.cleanComments(line);
      if (this.hasLabel(line)) {
        result = true;
        // console.log("beforeSaveLabel: line ->> " + line);
        this.fillEquivalenceTable(this.getLabel(line));
      }
    }
    return result;
  }

  public hasLabel (line: string): boolean {
    // console.log("hasLabel: line ->> " + line);
    let result = false;
    if (line.search(":") !== -1) {
      result = true;
    } else {
      const words = this.getUtilWords(line);
      for (const word of words) {
        // console.log("hasLabel: word ->> " + word);
        if ((!this.hasAsmInstruction(words[0]) && !this.hasMicrocontrollerInstruction(words[0]))) {
          if ((!this.hasAsmInstruction(words[1]))) {  // && this.hasMicrocontrollerInstruction(words[1]))) {
            result = true;
          }
        }
      }
    }
    // console.log(result);
    return result;
  }



  public removeLabel (line: string): string {
    // console.log("removeLabel: line ->> " + line);
    const labelBeginning = line.search(":");
    if (labelBeginning !== -1) {
      const lines = line.split(":");
      line = lines[1];
    } else {
      const words = this.getUtilWords(line);
      line = "";
      for (let word = 1; word <= words.length; word++) {
        // console.log("removeLabel: word ->> " + words[word]);
        line += words[word] + this.markCaracter;
      }
    }
    // console.log("result line ->> " + line);
    return line;
  }

  public getLabel (line: string): string {
    // console.log("in getLabel: line ->>" + line);
    const labelBeginning = line.search(":");
    if (labelBeginning !== -1) {
      const lines = line.split(":");
      const label = lines[0];
      line = label + this.markCaracter + "EQU" + this.markCaracter + this.toBinary.toHex(this.instrucctionCounter);
      // line = lines[1];
    } else {
      const words = this.getUtilWords(line);
      const label = words[0];
      line = label + this.markCaracter + "EQU" + this.markCaracter + this.toBinary.toHex(this.instrucctionCounter);
    }
    // console.log("out getLabel: line ->>" + line);
    return line;
  }

  public hasAsmInstruction (line: String): boolean {
    // console.log("hasAsmInstruction: line ->>" + line);
    let result = false;
    for (const asmInstruction of this.asmInstructionsList) {
      if (!isNullOrUndefined(line)) {
        if (line.search(asmInstruction) !== -1) {
          result = true;
        }
      }
    }
    return result;
  }

  public getAsmInstruction (line: string): string {
    // console.log("getAsmInstruction: line ->>" + line);
    let result = "false";
    for (const asmInstruction of this.asmInstructionsList) {
      if (!isNullOrUndefined(line)) {
        if (line.search(asmInstruction) !== -1) {
          result = asmInstruction;
        }
      }
    }
    // console.log("getAsmInstruction: ->>" + result);
    return result;
  }

  public getUtilWords (line: string): Array<string> {
    // console.log(line);
    const words = line.trim().split(this.markCaracter);
    const table = [];
    let tableCounter = 0;
    for ( const word of words) {
      if ((!isNullOrUndefined(word)) && (-1 === word.search(this.markCaracter) && (word !== "") && (-1 === word.indexOf(this.markCaracter)))) {
        table[tableCounter] = word;
        // console.log("[" + word + "]");
        tableCounter++;
      }
    }
    return table;
  }

  public hasMicrocontrollerInstruction(line: String): boolean {
    let result = false;
    if (!isNullOrUndefined(line)) {
      const words = line.split(this.markCaracter);
      for (const word of words) {
        if (this.instructionSet.verifyInstruction(word)) {
          result = true;
        }
      }
    } else {
      this.error = true;
    }
    return result;
  }

  public getMicrocontrollerInstruction(line: String): string {
    const words = line.split(this.markCaracter);
    let result = "";
    for (const word of words) {
      if (this.instructionSet.verifyInstruction(word)) {
        result = word;
      }
    }
    return result;
  }

  public pushInstruction(line: string) {
    const words = this.getUtilWords(line);
    const firstWord = words[0];
    const secondWord = words[1];
    let operands = [];
    if (!isNullOrUndefined(secondWord) && (secondWord !== "")) {
      operands = secondWord.split(",");
    }
    // this.debuggArray("words push Instructions", words);
    // this.debuggArray("operands push Instructions", operands);
    const instructionModel = this.instructionSet.getInstructionDetails(firstWord);
    const nmonic = instructionModel.getMnemonic();
    const description = instructionModel.getDescription();
    const cycles = instructionModel.getCycles();
    const opCode = instructionModel.getOpCode();
    const opCode14 = instructionModel.getOpCode14();
    const instruction = new Instruction(nmonic, operands, description, cycles, opCode, opCode14);
    const memoryPosition = this.toBinary.toHex(++this.instrucctionCounter);
    this.instructions.set(memoryPosition + ": {" + (this.instrucctionCounter) + "} " + "line: {" + this.lineCounter + "}", instruction);
  }

  public fillEquivalenceTable(line: string) {
    const words = this.getUtilWords(line);
    // console.log(line);
    if (words[0] !== words[2]) {
      if (this.equivalenceTable.has(words[0])) {
        // console.log("equtable before existing[ " + this.equivalenceTable.get(words[0]) + " ]");
        // console.log("filling equtable existing[ " + words[0] + " ]=[ " + words[2] + " ]");
        // this.equivalenceTable[words[0]] = words[2];
        this.equivalenceTable.set(words[0], words[2]);
        // console.log("equtable after existing[ " + this.equivalenceTable.get(words[0]) + " ]");
      } else {
        // console.log("equtable before new [ " + this.equivalenceTable.get(words[0]) + " ]");
        // console.log("filling equtable new [ " + words[0] + " ]=[ " + words[2] + " ]");
        this.equivalenceTable.set(words[0], words[2]);
        // console.log("equtable before new [ " + this.equivalenceTable.get(words[0]) + " ]");
      }
    }
  }

  public getOutput() {
    return this.outputCode;
  }

  public getRealCode() {
    // console.log(this.realCode);
    return this.realCode;
  }

  public getEquTable(): string {
    let equTable = "";
    // console.log("------- EQU TABLE --------");
    this.equivalenceTable.forEach((address: string, label: string) => {
      // console.log(label + " --- " + address);
      equTable += "[  " + address  + "  ] [ " + label + " ]\n";
      // equTable += "[ " + this.fillLeftZeros(parseInt(parseInt(address, 2) + "", 10 ).toString(16).toUpperCase(), 4) + " ] [ " + label + " ]\n";
    });
    return equTable;
  }

  public generateInstructions () {
    // console.log("------- INSTRUCTIONS --------");
    let counter = 1;
    this.instructions.forEach((instruction: Instruction, key: String) => {
      const operands = instruction.getOperands();
      // console.log("####################    " + counter + "   ####################");
      // console.log("instruction: [ " + instruction.getMnemonic() + " ]");
      // console.log("[" + operands[0] + "]=" + "=[" + operands[1] + "]");
      if (this.equivalenceTable.has(operands[0])) {
        // console.log("equ equivalence [0]: [ " + this.equivalenceTable.get(operands[0]) + " ]");
        // console.log("equ equivalence [0]: [ " + this.equivalenceTable.get(operands[0]) + " ]");
        // console.log("equ to binary [1]: [ " + this.toBinary.toBinaryNumber(this.equivalenceTable.get(operands[0]) + " ]"));
        // console.log("equ to binary [1]: [ " + this.toBinary.toBinaryNumber(this.equivalenceTable.get(operands[0]) + " ]"));
        operands[0] = this.toBinary.toBinaryNumber(this.equivalenceTable.get(operands[0]));
        operands[0] = this.toBinary.fillLeftZeros(operands[0], 14);
        if (this.equivalenceTable.has(operands[1])) {
          // console.log("si tiene equ: [ " + (this.equivalenceTable.get(operands[1])) + " ]");
          operands[1] = this.toBinary.toBinaryNumber(this.equivalenceTable.get(operands[1]));
          operands[1] = this.toBinary.fillLeftZeros(operands[1], 14);
        } else {
          operands[1] = this.toBinary.toBinaryNumber(operands[1]);
          operands[1] = this.toBinary.fillLeftZeros(operands[1], 14);
        }
      } else {
        operands[0] = this.toBinary.toBinaryNumber(operands[0]);
        operands[0] = this.toBinary.fillLeftZeros(operands[0], 14);
      }
      // console.log("[" + operands[0] + "]=" + "=[" + operands[1] + "]");
      counter++;
      // console.log("######################################################");
      instruction.setOperands(operands);
      instruction.fillOpcode14();
    });
  }


  public getInstructions (): string {
    let instructions = "";
    // console.log("------- INSTRUCTIONS --------");
    this.instructions.forEach((instruction: Instruction, key: String) => {
      // console.log( instruction.getMnemonic() + "--> " + instruction.toString());
      instructions +=   key + "\n" +
      "Mnemonic: " + instruction.getMnemonic() + "\n" +
      "FirstOperand: " + instruction.getFirstOperand() + "\n" +
      "SecondOperand: " + instruction.getSecondOperand() + "\n" +
      "OpCode14Format: " + instruction.getOpCode14Format() + "\n" +
      "OpCode14: " + instruction.getOpCode14() + "\n" +
      this.toBinary.fillLeftZeros(parseInt(parseInt(instruction.getOpCode14(), 2) + "", 10 ).toString(16).toUpperCase(), 4) + "\n" +
      "---------------------------------------\n";
    });
    return instructions;
  }

  public getInstructionsMap (): string {
    let instructions = "";
    // console.log("------- INSTRUCTIONS --------");
    this.instructions.forEach((instruction: Instruction, key: String) => {
      // console.log( instruction.getMnemonic() + "--> " + instruction.toString());
      instructions +=
      instruction.getFirstOperand() + "-" +
      instruction.getSecondOperand() + "-" +
      instruction.getOpCode14Format() + "-" +
      instruction.getOpCode14() + "-" +
      this.toBinary.fillLeftZeros(parseInt(parseInt(instruction.getOpCode14(), 2) + "", 10 ).toString(16).toUpperCase(), 4) + "\n";
    });
    return instructions;
  }

  public getHexInstructions (): string {
    let instructions = "";
    // console.log("------- INSTRUCTIONS --------");
    this.instructions.forEach((instruction: Instruction, key: String) => {
      // console.log( instruction.getMnemonic() + "--> " + instruction.toString());
      instruction.fillOpcode14();
      instructions += this.toBinary.fillLeftZeros(parseInt(parseInt(instruction.getOpCode14(), 2) + "", 10 ).toString(16).toUpperCase(), 4) + "\n";
    });
    return instructions;
  }

  public getOutputCode () {
    console.log(this.outputCode);
  }

  public getBinaryCode() {
    let binaryCode = this.binaryCode;
     // console.log("------- INSTRUCTIONS --------");
     this.instructions.forEach((instruction: Instruction, key: String) => {
       // console.log( instruction.getMnemonic() + "--> " + instruction.toString());
       instruction.fillOpcode14();
       binaryCode +=  "{" + instruction.getOpCode14() + "}  -- " + key + "\n";
     });
    return binaryCode;
  }

  public debuggArray (message: string, dArray: string[]) {
    let index = 0;
    for (const item of dArray) {
      console.log(message + "[ " + index++ + " ] >>> {" + item + "}");
    }
  }

}
