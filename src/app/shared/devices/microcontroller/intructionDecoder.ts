import { InstructionSet } from "../../compiler/instructionSet";
import { ToBinaryService } from "../../../services/toBinary.service";
import { Instruction } from "../../compiler/instruction";
import { isNullOrUndefined } from "util";
import { forEach } from "@angular/router/src/utils/collection";

export class InstructionDecoder {

  private instructionsSet;
  private instructionMap = new Map<string, string>();
  private model: string;
  private instruction: Instruction;
  private parameters;
  private opcodeSize: number;
  private binaryInstruction: string;
  private toBinary: ToBinaryService;

  public constructor(model: string) {
    this.model = model;
    this.opcodeSize = 14;
    this.parameters = [];
    this.instructionsSet = new InstructionSet(this.model).getInstructionsSet();
    this.toBinary = new ToBinaryService();

  }

  public decodeInstruction ( instructionCode: string ): string[] {
    console.log(`DecodeInstruction [${instructionCode}]`);
    // console.log(this.instructionMap);
    const operation = [];
    operation["instruction"] = "not match";
    const hexFormatInstructionCode = "0x" + instructionCode;
    // this.binaryInstruction = this.toBinary.toBinaryNumber(hexFormatInstructionCode);
    // this.binaryInstruction = this.toBinary.fillLeftZeros(this.binaryInstruction, this.opcodeSize);
    this.binaryInstruction = instructionCode;
    if (!isNullOrUndefined(this.instructionMap.get(instructionCode))) {
      this.parameters = this.instructionMap.get(instructionCode).split("-");
      this.instructionsSet.forEach((instruction: Instruction, key: String) => {
        this.instruction = instruction;
        this.instruction.setOperands(this.parameters);
        this.instruction.fillOpcode14();
        // const hexCode = this.toBinary.fillLeftZeros(parseInt(parseInt(instruction.getOpCode14(), 2) + "", 10 ).toString(16).toUpperCase(), 4);
        // console.log(`HEX [${hexCode}]||[${instructionCode}] 14 [${this.instruction.getOpCode14()}] mn [${this.instruction.getMnemonic()}]  mn [${this.instruction.getfirstParameter()}]`);
        if ( this.instruction.getOpCode14() === instructionCode) {
          operation["instruction"] = this.instruction.getMnemonic();
          operation["opcode14"] = this.instruction.getOpCode14();
          operation["opcode14Format"] = this.instruction.getOpCode14Format();
          operation["firstParameter"] = this.parameters[0];
          operation["secondParameter"] = this.parameters[1];
        }
      });
    }
    return operation;
  }

  public setInstructionMap(instructionMap: Map<string, string>) {
    this.instructionMap = instructionMap;
  }
}
