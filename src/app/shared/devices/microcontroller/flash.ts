import { Memory } from "./memory";

export class Flash extends Memory {

  constructor(memorySize: number, wordSize: number) {
    const name = "FLASH";
    super(name, memorySize, wordSize);
  }


  // public getAllRegisters(): any {
  //   // tslint:disable-next-line:prefer-const
  //   let memoryRegisters = getMemoryRegisters();
  //   // tslint:disable-next-line:prefer-const
  //   let flashMemoryRegisters = [];
  //   for ( let address = 0; address < this.getSize(); address++) {
  //     // tslint:disable-next-line:prefer-const
  //     let memoryRegister = {address: 0, name: "", hexValue: "", binValue: ""};
  //     // tslint:disable-next-line:prefer-const
  //     let register = memoryRegisters.get(address);
  //     memoryRegister.address = address;
  //     memoryRegister.name = register.getName();
  //     memoryRegister.binValue = parseInt(register.getValue(), 2).toString(16).toUpperCase();
  //     memoryRegister.hexValue = register.getValue();
  //     // console.log(memoryRegister);
  //     flashMemoryRegisters.push(memoryRegister);
  //   }
  //   // console.log(flashMemoryRegisters);
  //   return memoryRegisters;
  // }
}
