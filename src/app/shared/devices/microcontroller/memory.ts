import { MemoryRegister } from "./memory-register";
import { isNullOrUndefined } from "util";

export class Memory {
  private name: string;
  private wordSize: number;
  private memorySize: number;
  private memoryRegisters = new Map<number, MemoryRegister>();

  public constructor(name: string, memorySize: number, wordSize: number) {
    this.name = name;
    this.memorySize = memorySize;
    this.wordSize = wordSize;
  }

  public getSize(): number {
    return this.memorySize;
  }

  public getWordSize(): number {
    return this.wordSize;
  }

  public createNewRegister(address: number, name: string, value: string, wordSize: number, valueReset: string, bitsDescription: string[], description: string) {
    wordSize = this.wordSize;
    const register = new MemoryRegister(address, name, value, wordSize, valueReset, bitsDescription, description);
    this.memoryRegisters.set(address, register);
  }

  public setRegisterCompleteInfo(address: number, name: string, valueReset: string, bitsDescription: string[], description: string) {
    const register = this.memoryRegisters.get(address);
    if (!isNullOrUndefined(register)) {
      register.setName(name);
      register.setValueReset(valueReset);
      register.setbitsDescription(bitsDescription);
      register.setDescription(description);
    }
  }

  public setRegisterValue(address: number, value: string): boolean {
    let result = false;
    // this.showAllRegisters();
    console.log("address: [" + address + "]" + "address: [" + value + "]");
    console.log("memory size: [" + this.memorySize + "]");
    if (address < this.memorySize && address >= 0) {
      const register = this.memoryRegisters.get(address);
      console.log(register);
      register.setValue(value);
      this.memoryRegisters.set(address, register);
      result = true;
    }
    return result;
  }

  public setRegisterBitValue(address: number, bitPosition: number, value: string): boolean {
    let result = false;
    // this.showAllRegisters();
    console.log("address: [" + address + "]" + "value: [" + value + "]");
    // console.log("memory size: [" + this.memorySize + "]");
    if (address < this.memorySize && address >= 0) {
      const register = this.memoryRegisters.get(address);
      console.log(register);
      register.setBitState(bitPosition, value);
      this.memoryRegisters.set(address, register);
      result = true;
    }
    return result;
  }

  public getRegisterValue(address: number) {
  let result;
  if (!isNullOrUndefined(address)) {
    console.log(`Memory GetRegisterValue-Address [${address}]`);
    result = this.memoryRegisters.get(address).getValue();
    console.log("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%");
    console.log("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%\n");
    console.log(this.memoryRegisters.get(address));
    console.log(result);
    console.log("\n%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%");
    console.log("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%");
  } else {
    console.log(`[[[ Memory Address is Undefined ]]]`);
    result = -1;
  }

  return result;
  }

  public getMemoryRegisters() {
    return this.memoryRegisters;
  }

  public initializeAllRegisters(value: string) {
    const name = "";
    const wordSize = this.wordSize;
    const valueReset = "";
    const bitsDescription = ["", "", "", "", "", "", "", "", ""];
    const description = "";
    for (let address = 0; address < this.memorySize; address++) {
      this.createNewRegister(address, name, value, wordSize, valueReset, bitsDescription, description);
    }
  }

  public showAllRegisters() {
    let memoryRegisters = "";
    memoryRegisters = "\n______ " + this.name + " ( Memory Registers ) ______\n";
    console.log("\n______ " + this.name + "( Memory Registers ) ______\n");
    this.memoryRegisters.forEach(
      (register: MemoryRegister, key: number) => {
        memoryRegisters += "[ " + key + " ] -- [ " + register.getValue() + " ]\n";
        console.log("[ " + key + " ] -- [ " + register.getValue() + " ]");
      }
    );
    return memoryRegisters;
  }

  public getAllRegisters(): any {
    // tslint:disable-next-line:prefer-const
    let memoryRegisters = [];
    for ( let address = 0; address < this.memoryRegisters.size; address++) {
      // tslint:disable-next-line:prefer-const
      let memoryRegister = {address: 0, name: "", hexValue: "", binValue: ""};
      // tslint:disable-next-line:prefer-const
      let register = this.memoryRegisters.get(address);
      memoryRegister.address = address;
      memoryRegister.name = register.getName();
      memoryRegister.hexValue = parseInt(register.getValue(), 2).toString(16).toUpperCase();
      memoryRegister.binValue = register.getValue();
      // console.log(memoryRegister);
      memoryRegisters.push(memoryRegister);
      // console.log(memoryRegisters[address]);
    }
    return memoryRegisters;
  }

}
