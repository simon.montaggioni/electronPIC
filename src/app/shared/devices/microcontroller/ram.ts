import { Memory } from "./memory";

export class Ram extends Memory {

  constructor(memorySize: number, wordSize: number) {
    const name = "RAM";
    super(name, memorySize, wordSize);
  }
}
