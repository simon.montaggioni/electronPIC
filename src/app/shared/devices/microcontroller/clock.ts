export class Clock {
  private pulse: boolean;
  private speed: number;

  public constructor(speed: number) {
    this.speed = speed;
  }
}
