import { Alu } from "./alu";
import { InstructionDecoder } from "./intructionDecoder";
import { ToBinaryService } from "../../../services/toBinary.service";
import { Stack } from "./stack";

export class Cpu {
  private alu: Alu;
  private decoder: InstructionDecoder;
  private stack: Stack;
  private model: string;
  private flashInstruction: string;
  private ramAddress: number;
  private ramValue: string;
  private addressDestination: string;
  private programCounter: number;
  private instruction: string;
  private nextInstruction: number;
  private opcode14: string;
  private clockCycles: number;
  private workingRegister: string;
  private zeroFlag: string;
  private aluData: any;


  public constructor(model: string, ramWordSize: number) {
    this.model = model;
    this.alu = new Alu(model, ramWordSize);
    this.decoder = new InstructionDecoder(model);
    this.stack = new Stack("stack", 10, 2);
    this.clockCycles = 0;
    this.workingRegister = "00000000";
    this.zeroFlag = "0";
    this.aluData = {ramAddress: 8,
                    ramValue: "00000000",
                    firstParameter: "00000000",
                    secondParameter: "00000000",
                    operationResult: "00000000",
                    resultDestination: "0",
                    pc: 1,
                    z: this.zeroFlag,
                    clk: 1 };
  }

  public getWorkingRegister() {
    return this.workingRegister;
  }

  public getZeroFlag() {
    return this.zeroFlag;
  }

  public readFlashInstruction( flashInstruction: string) {
    this.flashInstruction = flashInstruction;
  }

  public readRamAddress (): number {
    console.log(this.ramAddress);
    return this.ramAddress;
  }

  public setRamValue (ramValue: string) {
    this.ramValue = ramValue;
  }

  public writeRamAddress () {
    return this.ramAddress;
  }

  public writeRamValue () {
    return this.ramValue;
  }

  public setProgramCounter (programCounter: number) {
      this.programCounter = programCounter;
  }

  public saveInStack() {
    this.stack.pushInstruction(this.programCounter);
  }

  public getNextInstruction(): number {
    console.log(`CPU getNextInstruction: nextInstruction before [${this.nextInstruction}]`);
    if (this.nextInstruction === -1) {
      this.nextInstruction = (this.stack.popInstruction() + 1);
    }
    //  else {
    //   this.nextInstruction = this.programCounter + 1;
    // }
    console.log(`CPU getNextInstruction: nextInstruction [${this.nextInstruction}]`);
    return this.nextInstruction;
  }

  public setDecoderInstructionMap(instructionMap: Map<string, string>) {
    this.decoder.setInstructionMap(instructionMap);
  }

  public getClockCycles() {
    return this.clockCycles;
  }

  public decodeInstruction () {
    let operation = [];
    console.log(`CPU-Decode [${this.flashInstruction}]`);
    operation = this.decoder.decodeInstruction(this.flashInstruction);
    console.log(`CPU-Decode OPERATION[${operation}]`);
    this.instruction = operation["instruction"];
    if (operation["opcode14Format"].search("f") !== -1) {
      this.ramAddress = parseInt(operation["firstParameter"], 2);
      this.aluData.ramAddress = parseInt(operation["firstParameter"], 2);
    } else if (operation["opcode14Format"].search("k") !== -1) {
      this.ramAddress = -1;
      this.aluData.ramAddress = -1;
      console.log(`CPU-Decode OPERATION FIRST OPERAND [${operation["firstParameter"]}]`);
      this.ramValue = "xxxxxxxx"; // operation["firstParameter"];
      this.aluData.firstParameter = operation["firstParameter"];
    } else {
      this.ramAddress = -1;
    }
    this.addressDestination = operation["secondParameter"];
    this.aluData.secondParameter = operation["secondParameter"];
    this.opcode14 = operation["opcode14"];
    console.log(`RamAddress [${this.ramAddress}]`);
    console.log(`RamValue [${this.ramValue}]`);
  }

  public execInstruction(): string {
    this.zeroFlag = "0";
    console.log(`CPU execInstruction: instruction [${this.instruction}]`);
    if ((this.instruction === "GOTO") || (this.instruction === "CALL")) {
      console.log(`CPU execInstruction: opcode14 ${this.opcode14}`);
      console.log(this.opcode14.slice(3, this.opcode14.length));
      this.nextInstruction = parseInt(this.opcode14.slice(3, this.opcode14.length), 2);
      console.log(`CPU execInstruction: nextInstruction GOTO or CALL [${this.nextInstruction}]`);
      if (this.instruction === "CALL") {
        this.saveInStack();
      }
      this.clockCycles = 2;
      console.log(this.programCounter);
      console.log(this.nextInstruction);
    } else {
      console.log("CPU ALUDATA berfore");
      this.aluData.ramAddress = this.ramAddress;
      this.aluData.ramValue = this.ramValue;
      console.log("alu data " + this.aluData.ramAddress);
      console.log("alu data " + this.aluData.ramValue);
      console.log(this.aluData);
      console.log("WR" + this.alu.getWorkingRegisterValue());
      this.aluData = this.alu.execIntruction(this.instruction, this.aluData);
      console.log("CPU ALUDATA after");
      console.log(this.aluData);
      console.log("alu data " + this.aluData.ramAddress);
      console.log("alu data " + this.aluData.ramValue);
      console.log("WR" + this.alu.getWorkingRegisterValue());
      this.workingRegister = this.alu.getWorkingRegisterValue();
      this.ramAddress = this.aluData.ramAddress; // parseInt(this.aluData.ramAddress, 10);
      this.ramValue = this.aluData.ramValue;
      this.clockCycles = this.aluData.clk;
      this.zeroFlag = this.aluData.z;
      if (parseInt(this.aluData.pc, 10) === -1) {
        this.nextInstruction = -1;
      } else {
        console.log(`CPU execInstruction: programCounter [${this.programCounter}]`);
        console.log(`CPU execInstruction: data[ pc ] [${parseInt(this.aluData.pc, 10)}]`);
        this.nextInstruction = this.programCounter + parseInt(this.aluData.pc, 10);
        console.log(`CPU execInstruction: nextInstruction [${this.nextInstruction}]`);
      }
    }
    console.log("ramAddress[[[" + this.ramAddress + "]]]");
    console.log("ramValue[[[" + this.ramValue + "]]]");
    console.log("cpu Working Register[[[" + this.getWorkingRegister() + "]]]");
    console.log("zeroFlagValue[[[" + this.getZeroFlag() + "]]]");
    console.log(this.programCounter);
    console.log("NextInstruction:  " + this.nextInstruction);
    console.log("CPU CLOCK CYCLES:  " + this.clockCycles);

    return this.instruction;
  }

}
