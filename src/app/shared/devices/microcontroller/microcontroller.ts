import { Ram } from "./ram";
import { Flash } from "./flash";
import { Eeprom } from "./eeprom";
import { InstructionSet } from "../../compiler/instructionSet";
import { Cpu } from "./cpu";
import { isNullOrUndefined } from "util";
import { Device } from "../device";
export class Microcontroller extends Device {
  private instructionsSet;
  private instructions = new Map<string, string>();
  private model: string;
  private microcontrollerFeatures = { flashSize: 35, ramSize: 200, eepromSize: 10, flashWordSize: 14, ramWordSize: 8, eepromWordSize: 8 };
  private code: string;
  private flashSize: number;
  private ramSize: number;
  private eepromSize: number;
  private flashWordSize: number;
  private ramWordSize: number;
  private eepromWordSize: number;
  private flash: Flash;
  private ram: Ram;
  private eeprom: Eeprom;
  private cpu: Cpu;
  private programCounter: number;
  private flashTopFilledRegister: number;
  private cycle: number;
  private cycleExecutionDetails: any;
  private clockCycles: number;
  private workingRegister: string;

  public constructor(model: string) {
    super("Microcontoller", 18);
    this.model = model;
    this.instructionsSet = new InstructionSet(model);
    this.flashSize = this.microcontrollerFeatures.flashSize;
    this.ramSize = this.microcontrollerFeatures.ramSize;
    this.eepromSize = this.microcontrollerFeatures.eepromSize;
    this.flashWordSize = this.microcontrollerFeatures.flashWordSize;
    this.ramWordSize = this.microcontrollerFeatures.ramWordSize;
    this.eepromWordSize = this.microcontrollerFeatures.eepromWordSize;

    this.flash = new Flash(this.flashSize, this.flashWordSize);
    this.ram = new Ram(this.ramSize, this.ramWordSize);
    this.eeprom = new Eeprom(this.eepromSize, this.eepromWordSize);
    this.setProgramCounter(0);
    this.cpu = new Cpu(model, this.ramWordSize);
    this.cycleExecutionDetails = {
      microcontrollerModel: "",
      programCounter: "",
      flashAddress: "",
      flashInstruction: "",
      currentInstruction: "",
      nextInstruction: "",
      readRamAddress: "",
      writeRamAddress: "",
      totalCycles: "",
    };

    this.workingRegister = "00000000";
    // Configuración de los registros FSR para PIC16F84a
    // this.ram.initializeAllRegisters("00000000");
    this.initializeRam("00000000");
    // Bank0
    this.ram.setRegisterCompleteInfo(0x00, "INDF", "--------", ["", "", "", "", "", "", "", ""], "Uses contents of FSR to address Data Memory (not a physical register)");
    this.ram.setRegisterCompleteInfo(0x01, "TMR0", "xxxxxxxx", ["", "", "", "", "", "", "", ""], "8-bit Real-Time Clock/Counter");
    this.ram.setRegisterCompleteInfo(0x02, "PCL", "00000000", ["", "", "", "", "", "", "", ""], "Low Order 8 bits of the Program Counter (PC)");
    this.ram.setRegisterCompleteInfo(0x03, "STATUS", "00011xxx", ["C", "DC", "Z", "PD", "TO", "RP0", "RP1", "IRP"], "Low Order 8 bits of the Program Counter (PC)");
    this.ram.setRegisterCompleteInfo(0x04, "FSR", "xxxxxxxx", ["", "", "", "", "", "", "", ""], "Indirect Data Memory Address Pointer 0");
    this.ram.setRegisterCompleteInfo(0x05, "PORTA", "---xxxxx", ["RA0", "RA1", "RA2", "RA3", "RA4/T0CKI", "-", "-", "-"], "");
    this.ram.setRegisterCompleteInfo(0x06, "PORTB", "xxxxxxxx", ["RB0/INT", "RB1", "RB2", "RB3", "RB4", "RB5", "RB6", "RB7"], "");
    this.ram.setRegisterCompleteInfo(0x07, "-", "-", ["", "", "", "", "", "", "", ""], "Unimplemented location, read as '0'");
    this.ram.setRegisterCompleteInfo(0x08, "EEDATA", "xxxxxxxx", ["", "", "", "", "", "", "", ""], "EEPROM Data Register");
    this.ram.setRegisterCompleteInfo(0x09, "EEADDR", "xxxxxxxx", ["", "", "", "", "", "", "", ""], "EEPROM Address Register");
    this.ram.setRegisterCompleteInfo(0x0a, "PCLATH", "---00000", ["", "", "", "", "", "-", "-", "-"], "Write Buffer for upper 5 bits of the PC(1)");
    this.ram.setRegisterCompleteInfo(0x0b, "INTCON", "0000000x", ["RBIF", "INTF", "T0IF", "RBIE", "INTE", "T0IE", "EEIE", "GIE"], "");
    // Bank1
    this.ram.setRegisterCompleteInfo(0x80, "INDF", "--------", ["", "", "", "", "", "", "", ""], "Uses contents of FSR to address Data Memory (not a physical register)");
    this.ram.setRegisterCompleteInfo(0x81, "OPTION_REG", "11111111", ["PS0", "PS1", "PS2", "PSA", "T0SE", "T0CS", "INTEDG", "RBPU"], "");
    this.ram.setRegisterCompleteInfo(0x82, "PCL", "00000000", ["", "", "", "", "", "", "", ""], "Low Order 8 bits of the Program Counter (PC)");
    this.ram.setRegisterCompleteInfo(0x83, "STATUS", "00011xxx", ["C", "DC", "Z", "PD", "TO", "RP0", "RP1", "IRP"], "Low Order 8 bits of the Program Counter (PC)");
    this.ram.setRegisterCompleteInfo(0x84, "FSR", "xxxxxxxx", ["", "", "", "", "", "", "", ""], "Indirect Data Memory Address Pointer 0");
    this.ram.setRegisterCompleteInfo(0x85, "TRISA", "---11111", ["", "", "", "", "", "-", "-", "-"], "PORTA Data Direction Register");
    this.ram.setRegisterCompleteInfo(0x86, "TRISB", "11111111", ["", "", "", "", "", "", "", ""], "PORTB Data Direction Register");
    this.ram.setRegisterCompleteInfo(0x87, "-", "-", ["", "", "", "", "", "", "", ""], "Unimplemented location, read as '0'");
    this.ram.setRegisterCompleteInfo(0x88, "EECON1", "---0x000", ["RD", "WR", "WREN", "WRERR", "EEIF", "-", "-", "-"], "");
    this.ram.setRegisterCompleteInfo(0x89, "EECON2", "--------", ["", "", "", "", "", "", "", ""], "EEPROM Control Register 2 (not a physical register)");
    this.ram.setRegisterCompleteInfo(0x0a, "PCLATH", "---00000", ["", "", "", "", "", "-", "-", "-"], "Write Buffer for upper 5 bits of the PC(1)");
    this.ram.setRegisterCompleteInfo(0x0b, "INTCON", "0000000x", ["RBIF", "INTF", "T0IF", "RBIE", "INTE", "T0IE", "EEIE", "GIE"], "");

    this.flash.initializeAllRegisters("00000000000000");
    // this.burnFlash(this.code);
    this.cpu.setDecoderInstructionMap(this.instructions);
    this.cycle = 0;
    this.clockCycles = 0;
     //  this.flash.setRegisterValue(0x0000, "RESET Vector", "00000000000000", "00000000000000");
     //  this.flash.setRegisterValue(0x0004, "Peripheral Interrupt Vector", "00000000000000", "00000000000000");
     //  this.flash.setRegisterValue(0x000E, "0x2E30");
     //  this.flash.setRegisterValue(0x000F, "0x3FFF");
     //  this.flash.setRegisterValue(0x0400, "129");
     //  this.flash.setRegisterValue(0x0801, "130");
     //  this.flash.setRegisterValue(0x03FF, "0x3FFF");
     //  this.flash.setRegisterValue(0x1FFF - 1, "0x2E30");
     //  this.flash.setRegisterValue(0x1FFF + 2, "0x2E30");

     // Reset del MC
     // reset();
  }

  public execCycle () {
    let flashInstruction = "";
    let ramValue = "";
    this.cycleExecutionDetails.programCounter = this.getProgramCounter();
    // while ((this.getProgramCounter() <= this.flashTopFilledRegister) && (cycle < 25)) {
    console.log("##################################### Cycle Start  [" + this.cycle + "] #####################################");
    console.log(`MICROCONTROLLER ExecProgram: [[[ PC: ${this.getProgramCounter()} instruction ${flashInstruction} ]]]\n`);
    console.log("#######################################################################################");
    // console.log(this.flash.showAllRegisters());
    console.log(`Microcontroller flashInstruction [${flashInstruction}]`);
    flashInstruction = this.flash.getRegisterValue(this.getProgramCounter());
    this.cpu.setProgramCounter(this.getProgramCounter());
    this.cpu.readFlashInstruction(flashInstruction);
    this.cpu.decodeInstruction();
    console.log(`Ram Value to READ [${this.cpu.readRamAddress()}] [${ramValue}]`);
    if (this.cpu.readRamAddress() !== -1) {
      ramValue = this.ram.getRegisterValue(this.cpu.readRamAddress());
      this.cpu.setRamValue(ramValue);
    }
    debugger
    const currentInstruction = this.cpu.execInstruction();
    this.clockCycles = this.clockCycles + this.cpu.getClockCycles();
    this.workingRegister = this.cpu.getWorkingRegister();
    if ("1" === this.cpu.getZeroFlag()) {
      this.ram.setRegisterBitValue(3, 2, "1"); // bit 2 of the status register is set to 1
    } else {
      this.ram.setRegisterBitValue(3, 2, "0"); // bit 2 of the status register is set to 0
    }
    console.log(`MICROCONTROLLER WORKING REGISTER [[${this.workingRegister}]]`);
    if (this.cpu.writeRamAddress() !== -1 ) {
      // console.log(`Value to Store [${this.cpu.writeRamValue()}]`);
      // console.log(`Ram Value Before [${this.cpu.writeRamAddress()}]||[${this.ram.getRegisterValue(this.cpu.writeRamAddress())}]`);
      this.ram.setRegisterValue(this.cpu.writeRamAddress(), this.cpu.writeRamValue());
      // console.log(`Ram Value After [${this.cpu.writeRamAddress()}]||[${this.ram.getRegisterValue(this.cpu.writeRamAddress())}]`);
      // this.ram.setRegisterBitValue(3, 2, this.cpu.getZeroFlag());
    }
    this.setProgramCounter(this.cpu.getNextInstruction());
    console.log(`MICROCONTROLLER ExecProgram: getNextInstruction [${this.cpu.getNextInstruction()}]`);
    console.log("###############################################################################################\n");
    this.cycle++;
    this.cycleExecutionDetails.microcontrollerModel = this.model;
    this.cycleExecutionDetails.programCounter = this.getProgramCounter();
    this.cycleExecutionDetails.flashAddress = this.flash.getRegisterValue(this.getProgramCounter());
    this.cycleExecutionDetails.flashInstruction = flashInstruction;
    this.cycleExecutionDetails.currentInstruction = currentInstruction;
    this.cycleExecutionDetails.nextInstruction = this.getProgramCounter();
    this.cycleExecutionDetails.totalCycles = this.clockCycles.toString(10);
    // console.log("clockCycles" + this.clockCycles.toString(10) );
    // }
    // console.log(`MICROCONTROLLER ExecProgram: flashTopFilledRegister [${this.flashTopFilledRegister}]`);
  }

  public getCycleDetails(): any {
    return this.cycleExecutionDetails;
  }

  public runProgram (maxCyclesToExecute: number) {
    while ((this.getProgramCounter() <= this.flashTopFilledRegister) && (this.cycle < maxCyclesToExecute)) {
      this.execCycle();
    }
  }

  public reset() {
    this.clockCycles = 0;
    this.setProgramCounter(0);
  }

  public getProgramCounter (): number {
    return this.programCounter;
  }

  public setProgramCounter (value: number) {
    this.programCounter = value;
  }

  public increaseProgramCounter () {
    this.programCounter++;
  }

  public initializeRam (bitValues: string) {
     this.ram.initializeAllRegisters(bitValues);
  }

  public getRamRegisterValue (address: number): string {
    const registerValue = this.ram.getRegisterValue(address);
    return registerValue;
  }

  public setRamRegisterValue(address: number, value: string) {
    this.ram.setRegisterValue(address, value);
    const value2 = this.ram.getRegisterValue(address);
  }

  public initializeFlash(bitValues: string) {
    this.ram.initializeAllRegisters(bitValues);
  }

  public getFlashRegisterValue(address: number): string {
    const registerValue = this.flash.getRegisterValue(address);
    return registerValue;
  }

  public setFlashRegisterValue(address: number, value: string) {
    this.ram.setRegisterValue(address, value);
    this.ram.getRegisterValue(address);
  }

  public burnFlash(code: string): boolean {
    let result = false;
    let flashAddress = 0x0000;
    const codeLines = code.split("\n");
    let data = [];
    for (const line of codeLines) {
      data = line.split("-");
      console.log("###########################" + data + "##########################");
      if (!isNullOrUndefined(data[3])) {
        console.log("########################### [[[ " + flashAddress + " ]]] ##########################");
        this.flash.setRegisterValue(flashAddress, data[3]);
        this.instructions.set(data[3], data[0] + "-" + data[1]);
        this.flashTopFilledRegister = flashAddress;
        console.log("########################### [[[[[[ " + flashAddress + " ]]]]]] ##########################");
        console.log("########################### [[[[[[ " + this.flash.getRegisterValue(flashAddress) + " ]]]]]] ##########################");
        result = true;
      }
      flashAddress++;
    }
    return result;
  }
  public getFlashSize() {
    return this.flashSize;
  }

  public initializeEeprom(bitValues: string) {
    this.ram.initializeAllRegisters(bitValues);
  }

  public getEepromRegisterValue(address: number): string {
    const registerValue = this.ram.getRegisterValue(address);
    return registerValue;
  }

  public setEepromRegisterValue(address: number, value: string) {
    this.ram.setRegisterValue(address, value);
    const value2 = this.ram.getRegisterValue(address);
  }

  public displayRegisterValue(address: number, memory: string) {
    let value: string;
    switch (memory) {
      case "flash":
        value = this.getFlashRegisterValue(address);
        break;

      case "ram":
        value = this.getRamRegisterValue(address);
        break;

      case "eeprom":
        value = this.getEepromRegisterValue(address);
        break;

      default:
        break;
    }
    //  console.log("searchAddress:[" + address + "] -- [" + value + "]");
    // return "searchAddress:[" + address + "] -- [" + value + "]";
    return value;
  }

  public printAllRamValues() {
    return this.ram.showAllRegisters();
  }

  public getAllRamValues() {
    return this.ram.getAllRegisters();
  }

  public printAllFlashValues() {
    return this.flash.showAllRegisters();
  }

  public getAllFlashValues() {
    return this.flash.getAllRegisters();
  }

  public setPortState (portName: string, pinsState: string) {
    switch (portName) {
      case "A":
        this.ram.setRegisterValue(5, pinsState);
        break;
      case "B":
        this.ram.setRegisterValue(6, pinsState);
        break;
      default:
        break;
    }
  }

  public getPortState (portName: string): string {
    let port: string;
    switch (portName) {
      case "A":
        port = this.ram.getRegisterValue(5);
        break;
      case "B":
        port = this.ram.getRegisterValue(6);
        break;
      default:
        break;
    }
    return port;
  }

  public setPortPinState (portName: string, pinNumber: number, pinState: boolean) {
    let portValue: string;
    let portArray: string[];
    switch (portName) {
      case "A":
        portArray = this.convertRegister2Array(this.ram.getRegisterValue(5));
        portArray[pinNumber] = (pinState) ? "1" : "0";
        portValue = this.convertArray2Register(portArray);
        this.ram.setRegisterValue(5, portValue);
        break;
      case "B":
        portArray = this.convertRegister2Array(this.ram.getRegisterValue(6));
        portArray[pinNumber] = (pinState) ? "1" : "0";
        portValue = this.convertArray2Register(portArray);
        this.ram.setRegisterValue(5, portValue);
        break;
      default:
        break;
    }
  }

  public convertRegister2Array(register: string): string[] {
   const registerArray = [];
   const registerLength = register.length;
    for ( let i = 0; i < registerLength; i++) {
      registerArray[i] = register.charAt(i);
    }
   return registerArray;
  }

  public convertArray2Register(registerArray: string[]): string {
   console.log(registerArray);
   let register = "";
   const registerArrayLength = registerArray.length;
    for ( let i = 0; i < registerArrayLength; i++) {
      register = register + registerArray[i];
    }
   return register;
  }

  public getWorkingRegister() {
    return this.workingRegister;
  }

}
