import { ToBinaryService } from "../../../services/toBinary.service";
import { isNullOrUndefined } from "util";

export class Alu {
  private workingRegister: string;
  private model: string;
  private toBinary: ToBinaryService;
  private wordSize: number;
  private zeroFlag: boolean;
  private aluData: any;
  private firstOperand: number;
  private secondOperand: number;
  private result: number;
  private instruction: string;

  public constructor (model: string, wordSize: number) {
    this.model = model;
    this.wordSize = wordSize;
    this.toBinary = new ToBinaryService();
    this.setWorkingRegisterValue("11111111");
    this.instruction = "";
    this.aluData = {ramAddress: 0,
                    ramValue: "00000000",
                    firstParameter: "00000000",
                    secondParameter: "00000000",
                    operationResult: "00000000",
                    resultDestination: "0",
                    pc: 1,
                    z: this.zeroFlag,
                    clk: 0};
  }

  public execIntruction (instruction: string, aluData: any) {
    console.log("ALU DATA parameter ");
    console.log(aluData);
    this.instruction = instruction;
    this.aluData = aluData;
    this.aluData.pc = 1;
    this.aluData.clk = 1;
    this.aluData.z = "0";
    if (isNullOrUndefined(this.aluData.secondParameter)) {
      this.aluData.secondParameter = "0";
    }
    if (this.aluData.ramAddress !== -1) {
      this.firstOperand = parseInt(this.aluData.ramValue, 2);
    } else {
      this.firstOperand = parseInt(this.aluData.firstParameter, 2);
    }
    this.secondOperand = parseInt(this.getWorkingRegisterValue(), 2);
    this.aluData.resultDestination = parseInt(this.aluData.secondParameter, 2);
    this.instructionInfo();

    switch (this.instruction) {  // pendiente afectar registros C,DC,Z
      case "ADDWF":
        this.addwf();
        break;
      case "ANDWF":
        this.andwf();
        break;
      case "CLRF":
        this.clrf();
        break;
      case "CLRW":
        this.clrw();
        break;
      case "COMF":
        this.comf();
        break;
      case "DECF":
        this.decf();
        break;
      case "DECFSZ":
        this.decfsz();
        break;
      case "INCF":
        this.incf();
        break;
      case "INCFSZ":
        this.incfsz();
        break;
      case "IORWF":
        this.iorwf();
        break;
      case "MOVF":
        this.movf();
        break;
      case "MOVWF":
        this.movwf();
        break;
      case "NOP":
        this.nop();
        break;
      case "RLF":
        this.rlf();
        break;
      case "RRF":
        this.rrf();
        break;
      case "SUBWF":
        this.subwf();
        break;
      case "SWAPF":
        this.swapf();
        break;
      case "XORWF":
        this.xorwf();
        break;
      case "BCF":
        this.bcf();
        break;
      case "BSF":
        this.bsf();
        break;
      case "BTFSC":
        this.btfsc();
        break;
      case "BTFSS":
        this.btfss();
        break;
      case "ADDLW":
        this.addlw();
        break;
      case "ANDLW":
        this.andlw();
        break;
      case "CALL":
        this.call();
        break;
      case "CLRWDT":
        this.clrwdt();
        break;
      case "GOTO":
        this.goto();
        break;
      case "IORLW":
        this.iorlw();
        break;
      case "MOVLW":
        this.movlw();
        break;
      case "RETFIE":
        this.retfie();
        break;
      case "RETLW":
        this.retlw();
        break;
      case "RETURN":
        this.return();
        break;
      case "SLEEP":
        this.sleep();
        break;
      case "SUBLW":
        this.sublw();
        break;
      case "XORLW":
        this.xorlw();
        break;
      default:
        console.log(`ALU ERROR: Not recognized instruction [ ${instruction} ]`);
        break;
    }
    return this.aluData;
  }

  public getWorkingRegisterValue(): string {
    return this.workingRegister;
  }

  public setWorkingRegisterValue(workingRegisterValue: string) {
    this.workingRegister = workingRegisterValue;
  }

  private addwf () {
    this.result = this.firstOperand + this.secondOperand;
    this.aluData.operationResult = this.toBinaryResult(this.result);
    if (0 === this.aluData.resultDestination) {
      this.setWorkingRegisterValue(this.aluData.operationResult);
    } else {
      this.aluData.ramValue = this.aluData.operationResult;
    }

    this.setZeroFlag(this.aluData.operationResult);
  }

  private andwf () {
    this.result = this.firstOperand & this.secondOperand;
    this.aluData.operationResult = this.toBinaryResult(this.result);
    if (0 === this.aluData.resultDestination) {
      this.setWorkingRegisterValue(this.aluData.operationResult);
    } else {
      this.aluData.ramValue = this.aluData.operationResult;
    }

    this.setZeroFlag(this.aluData.operationResult);
  }

  private clrf () {
    this.aluData.operationResult = this.toBinaryResult(0);
    this.aluData.ramValue = this.aluData.operationResult;
    this.setZeroFlag(this.aluData.operationResult);
  }

  private clrw () {
    this.aluData.operationResult = this.toBinaryResult(0);
    this.setWorkingRegisterValue(this.toBinaryResult(0));

    this.setZeroFlag(this.aluData.operationResult);
  }

  private comf () {
    this.result = ~this.firstOperand;
    this.aluData.operationResult = this.toBinaryResult(this.result);
    if (0 === this.aluData.resultDestination) {
      this.setWorkingRegisterValue(this.aluData.operationResult);
    } else {
      this.aluData.ramValue = this.aluData.operationResult;
    }

    this.setZeroFlag(this.aluData.operationResult);
  }

  private decf () {
    this.result = this.firstOperand - 1;
    if ( this.result < 0) {
      this.result = 255;
    }

    this.aluData.operationResult = this.toBinaryResult(this.result);
    if (0 === this.aluData.resultDestination) {
      this.setWorkingRegisterValue(this.aluData.operationResult);
    } else {
      this.aluData.ramValue = this.aluData.operationResult;
    }

    this.setZeroFlag(this.aluData.operationResult);
  }

  private decfsz () {
    this.result = this.firstOperand - 1;

    if ( this.result < 0) {
      this.aluData.pc = 2;
      this.aluData.clk = 2;
      this.result = 255;
    }

    this.aluData.operationResult = this.toBinaryResult(this.result);

    if (0 === this.aluData.resultDestination) {
      this.setWorkingRegisterValue(this.aluData.operationResult);
    } else {
      this.aluData.ramValue = this.aluData.operationResult;
    }
  }

  private incf () {
    this.result = this.firstOperand + 1;
    if ( this.result > 255) {
      this.result = 0;
    }
    this.aluData.operationResult = this.toBinaryResult(this.result);
    if (0 === this.aluData.resultDestination) {
      this.setWorkingRegisterValue(this.aluData.operationResult);
    } else {
      this.aluData.ramValue = this.aluData.operationResult;
    }

    this.setZeroFlag(this.aluData.operationResult);
  }

  private incfsz () {
    this.result = this.firstOperand + 1;
    if ( this.result > 255) {
      this.aluData.pc = 2;
      this.aluData.clk = 2;
      this.result = 0;
    }
    this.aluData.operationResult = this.toBinaryResult(this.result);
    if (0 === this.aluData.resultDestination) {
      this.setWorkingRegisterValue(this.aluData.operationResult);
    } else {
      this.aluData.ramValue = this.aluData.operationResult;
    }
  }

  private iorwf () {
    this.result = this.firstOperand | this.secondOperand;
    this.aluData.operationResult = this.toBinaryResult(this.result);
    if (0 === this.aluData.resultDestination) {
      this.setWorkingRegisterValue(this.aluData.operationResult);
    } else {
      this.aluData.ramValue = this.aluData.operationResult;
    }

    this.setZeroFlag(this.aluData.operationResult);
  }

  private movf () {
    this.aluData.operationResult = this.toBinaryResult(this.firstOperand);
    if (0 === this.aluData.resultDestination) {
      this.setWorkingRegisterValue(this.aluData.operationResult);
    } else {
      this.aluData.ramValue = this.aluData.operationResult;
    }

    this.setZeroFlag(this.aluData.operationResult);
  }

  private movwf () {
    this.aluData.operationResult = this.toBinaryResult(this.secondOperand);
    this.aluData.ramValue = this.aluData.operationResult;
  }

  private nop () {
  }

  private rlf () {
    this.result = this.firstOperand << 1;
    this.aluData.operationResult = this.toBinaryResult(this.result);
    if (0 === this.aluData.resultDestination) {
      this.setWorkingRegisterValue(this.aluData.operationResult);
    } else {
      this.aluData.ramValue = this.aluData.operationResult;
    }
  }

  private rrf () {
    this.result = this.firstOperand >> 1;
    this.aluData.operationResult = this.toBinaryResult(this.result);
    if (0 === this.aluData.resultDestination) {
      this.setWorkingRegisterValue(this.aluData.operationResult);
    } else {
      this.aluData.ramValue = this.aluData.operationResult;
    }
  }

  private subwf () {
    this.result = this.firstOperand - this.secondOperand;
    this.aluData.operationResult = this.toBinaryResult(this.result);
    if (0 === this.aluData.resultDestination) {
      this.setWorkingRegisterValue(this.aluData.operationResult);
    } else {
      this.aluData.ramValue = this.aluData.operationResult;
    }

    this.setZeroFlag(this.aluData.operationResult);
  }

  private swapf () {
    const temporal = this.toBinaryResult(this.firstOperand);
    this.aluData.operationResult = temporal.slice(temporal.length / 2, temporal.length) + temporal.slice(0, length / 2);
    if (0 === this.aluData.resultDestination) {
      this.setWorkingRegisterValue(this.aluData.operationResult);
    } else {
      this.aluData.ramValue = this.aluData.operationResult;
    }
  }

  private xorwf () {
    this.result = this.firstOperand ^ this.secondOperand;
    this.aluData.operationResult = this.toBinaryResult(this.result);
    if (0 === this.aluData.resultDestination) {
      this.setWorkingRegisterValue(this.aluData.operationResult);
    } else {
      this.aluData.ramValue = this.aluData.operationResult;
    }

    this.setZeroFlag(this.aluData.operationResult);
  }

  private bcf () {
    const bitArray = [];
    const temporal = this.toBinaryResult(this.firstOperand);
    for (let i = 0; i < temporal.length; i++) {
        bitArray[i] = temporal.charAt(i);
    }
    bitArray[parseInt(this.aluData.secondParameter, 2)] = "0";
    this.aluData.operationResult = bitArray.toString().replace(/,/gi, "");
    this.aluData.ramValue = this.aluData.operationResult;
  }

  private bsf () {
    const bitArray = [];
    const temporal = this.toBinaryResult(this.firstOperand);
    for (let i = 0; i < temporal.length; i++) {
        bitArray[i] = temporal.charAt(i);
    }
    bitArray[parseInt(this.aluData.secondParameter, 2)] = "1";
    this.aluData.operationResult = bitArray.toString().replace(/,/gi, "");
    this.aluData.ramValue = this.aluData.operationResult;
  }

  private btfsc () {
    const bitArray = [];
    const temporal = this.toBinaryResult(this.firstOperand);
    for (let i = 0; i < temporal.length; i++) {
        bitArray[i] = temporal.charAt(i);
    }
    if (bitArray[parseInt(this.aluData.secondParameter, 2)] === "0") {
      this.aluData.pc = 2;
      this.aluData.clk = 2;
    }
  }

  private btfss () {
    const bitArray = [];
    const temporal = this.toBinaryResult(this.firstOperand);
    for (let i = 0; i < temporal.length; i++) {
        bitArray[i] = temporal.charAt(i);
    }
    if (bitArray[parseInt(this.aluData.secondParameter, 2)] === "1") {
      this.aluData.pc = 2;
      this.aluData.clk = 2;
    }
  }

  private addlw () {
    this.result = this.firstOperand + this.secondOperand;
    this.aluData.operationResult = this.toBinaryResult(this.result);
    this.setWorkingRegisterValue(this.aluData.operationResult);
    this.setZeroFlag(this.aluData.operationResult);
  }

  private andlw () {
    this.result = this.firstOperand & this.secondOperand;
    this.aluData.operationResult = this.toBinaryResult(this.result);
    this.setWorkingRegisterValue(this.aluData.operationResult);
    this.setZeroFlag(this.aluData.operationResult);
  }

  private call () {
    this.aluData.clk = 2;
  }

  private clrwdt () {
  }

  private goto () {
    this.aluData.clk = 2;
  }

  private iorlw () {
    this.result = this.firstOperand | this.secondOperand;
    this.aluData.operationResult = this.toBinaryResult(this.result);
    this.setWorkingRegisterValue(this.aluData.operationResult);
    this.setZeroFlag(this.aluData.operationResult);
  }

  private movlw () {
      console.log(`WORKING REGISTER MOVLW ${this.firstOperand}`);
      this.setWorkingRegisterValue(this.toBinaryResult(this.firstOperand));
      console.log(`WORKING REGISTER MOVLW ${this.getWorkingRegisterValue()}`);
  }

  private retfie () {
    this.aluData.pc = -1;
    this.aluData.clk = 2;
  }

  private retlw () {
    this.aluData.pc = -1;
    this.aluData.clk = 2;
    this.setWorkingRegisterValue(this.toBinaryResult(this.firstOperand));
  }

  private return () {
    this.aluData.pc = -1;
    this.aluData.clk = 2;
  }

  private sleep () {
  }

  private sublw () {
    this.result = this.firstOperand - this.secondOperand;
    this.aluData.operationResult = this.toBinaryResult(this.result);
    console.log(`SUBLW RESULT [[[${this.result}]]][[[${this.aluData.operationResult}]]]`);
    this.setWorkingRegisterValue(this.aluData.operationResult);
    this.setZeroFlag(this.aluData.operationResult);
  }

  private xorlw () {
    this.result = this.firstOperand ^ this.secondOperand;
    this.aluData.operationResult = this.toBinaryResult(this.result);
    this.setWorkingRegisterValue(this.aluData.operationResult);
    this.setZeroFlag(this.aluData.operationResult);
  }

  private setZeroFlag(register: string) {
    if (0 === parseInt(register, 10)) {
      this.aluData.z = "1";
    }
  }

  private toBinaryResult( number: number): string {
    let binaryResult = this.toBinary.toBinaryNumber(number.toString());
    binaryResult = this.toBinary.fillLeftZeros(binaryResult, this.wordSize);
    return binaryResult;
  }

  private instructionInfo() {
    console.log("\n\n***************************************************\n\n");
    console.log(this.instruction.toUpperCase() + ": ");
    console.log(this.aluData);
    console.log("\n\n***************************************************\n\n");
  }

}
