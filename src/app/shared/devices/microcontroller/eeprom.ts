import { Memory } from "./memory";

export class Eeprom extends Memory {

  constructor(memorySize: number, wordSize: number) {
    const name = "EEPROM";
    super(name, memorySize, wordSize);
  }
}
