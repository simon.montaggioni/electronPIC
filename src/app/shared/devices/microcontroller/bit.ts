export class Bit {
  private name: string;
  private number: number;
  private state: string;
  private type: string;

  constructor(name: string, number: number, type: string) {
    this.setName(name);
    this.setNumber(number);
    this.setState("0");
    this.setType(type);
  }

  public setName(name: string) {
    this.name = name;
  }

  public getName(): string {
    return this.name;
  }

  private setNumber(number: number) {
    this.number = number;
  }

  public getNumber(): number {
    return this.number;
  }

  public setState(state: string) {
    this.state = state;
  }

  public getState(): string {
    return this.state;
  }

  public setType(type: string) {
    this.type = type;
  }

  public getType(): string {
    return this.type;
  }

  public changeState() {
    this.state = (this.state === "1") ? "0" : "1";
  }


}
