import { Bit } from "./bit";

export class MemoryRegister {
         private address: number;
         private name: string;
         private bits: Bit[];
         private bitsValues: string;
         private length: number;
         private valueReset: string;
         private bitsDescription: string[];
         private description: string;

         public constructor(address: number, name: string, bitsValues: string, length: number, valueReset: string, bitsDescription: string[], description: string) {
           this.bits = [];
           this.address = address;
           this.name = name;
           this.length = length;
           this.valueReset = valueReset;
           this.description = description;

           if (bitsValues === "") {
             for (let bitPosition = 0; bitPosition < length; bitPosition++) {
               this.bits[bitPosition] = new Bit("", bitPosition, "0");
               this.bitsValues += (this.bits[bitPosition].getState()) ? "1" : "0" ;
             }
           } else {
             this.bitsValues = bitsValues;
             for (let bitPosition = 0; bitPosition < length; bitPosition++) {
               this.bits[bitPosition] = new Bit("", bitPosition, (bitsValues.charAt(bitPosition) === "1") ? "1" : "0" );
             }
           }
         }

         public getAddress(): number {
           return this.address;
         }

         public setAddress(address) {
           this.address = address;
         }

         public getName(): string {
           return this.name;
         }

         public setName(name: string) {
           this.name = name;
         }

         public getValue(): string {
           return this.bitsValues.slice(0, this.length);
         }

         public setValue(bits: string) {
           console.log(`MemoryRegister (SetValue): Register[${bits}]`);
           if (bits.length > length) {
             bits = this.trimBits(bits);
           }
           this.bitsValues = bits;
         }

         public bitArray2String() {
          this.bitsValues = "";
           for (let bitPosition = 0; bitPosition < this.bits.length; bitPosition++) {
            this.bitsValues += this.bits[bitPosition].getState();
           }
         }

         public bitString2Array() {
           for (let bitPosition = 0; bitPosition < this.bitsValues.length; bitPosition++) {
            this.bits[bitPosition].setState((this.bitsValues.charAt(bitPosition) === "1") ? "1" : "0");
           }
         }

         public getBitState(bitPosition: number): string {
           return (this.bits[bitPosition].getState()) ? "1" : "0";
         }

         public setBitState(bitPosition: number, bitState: string) {
           this.bits[bitPosition].setState((bitState === "1") ? "1" : "0");
           this.bitArray2String();
         }

         public getBits(): Bit[] {
           return this.bits;
         }

         public getLength(): number {
           return this.length;
         }

         public setLength(length: number) {
           this.length = length;
         }

         public getValueReset(): string {
           return this.valueReset;
         }

         public setValueReset(valueReset: string) {
           this.valueReset = valueReset;
         }

         public setbitsDescription(bitsDescription: string[]) {
           this.bitsDescription = bitsDescription;
         }

         public setDescription(description: string) {
           this.description = description;
         }

         public trimBits(bits: string): string {
           return (bits = bits.slice(bits.length - this.length, bits.length));
         }
       }
