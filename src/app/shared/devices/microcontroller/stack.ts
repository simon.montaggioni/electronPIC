import { MemoryRegister } from "./memory-register";
import { Memory } from "./memory";

export class Stack extends Memory {
  private instructionPosition: number;
  private stackSize: number;
  private stackLevel: number;
  private topFilledPosition: number;
  public constructor (name, stackSize, wordSize) {
    const memorySize = stackSize;
    super(name, memorySize, wordSize);
    this.instructionPosition = 0;
    this.topFilledPosition = 0;
    this.initializeAllRegisters("0");
  }

  public pushInstruction(instructionAddress: number) {
    console.log(this.showAllRegisters());
    console.log(`STACK pushInstruction: instructionAddress[${instructionAddress}]`);
    if (this.instructionPosition > this.stackSize) {
      this.instructionPosition = 0;
    }
    this.setRegisterValue(this.instructionPosition, instructionAddress.toString());
    console.log(this.showAllRegisters());
    this.topFilledPosition = this.instructionPosition;
    this.instructionPosition++;
  }

  public popInstruction(): number {
    // if (this.instructionPosition < 0) {
    //   this.instructionPosition = this.stackSize;
    // }
    console.log(this.showAllRegisters());
    console.log(`STACK popInstruction: instructionPosition [${this.topFilledPosition}]`);
    const instructionAddress = parseInt(this.getRegisterValue(this.topFilledPosition), 10);
    this.setRegisterValue(this.topFilledPosition, "0");
    console.log(`STACK popInstruction: instructionAddress [${instructionAddress}]`);
    this.instructionPosition--;
    console.log(this.showAllRegisters());
    return instructionAddress;
  }

}
