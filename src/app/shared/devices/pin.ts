export class Pin {
  private name: string;
  private number: number;
  private state: boolean;
  private type: boolean;

  constructor(name: string, number: number, type: boolean) {
    this.setName(name);
    this.setNumber(number);
    this.setState(false);
    this.setType(type);
  }

  public setName(name: string) {
    this.name = name;
  }

  public getName(): string {
    return this.name;
  }

  private setNumber(number: number) {
    this.number = number;
  }

  public getNumber(): number {
    return this.number;
  }

  public setState(state: boolean) {
    this.state = state;
  }

  public getState(): boolean {
    return this.state;
  }

  public setType(type: boolean) {
    this.type = type;
  }

  public getType(): boolean {
    return this.type;
  }

  public changeState() {
    this.state = (this.state) ? false : true;
  }

}
