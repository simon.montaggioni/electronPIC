import { Pin } from "./pin";
export class Device {
  private name: string;
  private type: string;
  private states: boolean[];
  private numberOfPins: number;
  private pins: Pin[];
  private images: string[];

  constructor(name: string, numberOfPins: number) {
    this.pins = [];
    this.name = name;
    this.numberOfPins = numberOfPins;
    for (let pinNumber = 0; pinNumber < numberOfPins; pinNumber++) {
      this.pins.push( new Pin("", pinNumber, true));
    }
    for (let pinNumber = 0; pinNumber < numberOfPins; pinNumber++) {
      this.setPinState( pinNumber, false);
    }
  }

  private setName(name: string) {
    this.name = name;
  }

  public getName(): string {
    return this.name;
  }

  public getNumberOfPins (): number {
    return this.numberOfPins;
  }

  private setPinName(pinNumber: number, pinName: string) {
    this.pins[pinNumber].setName(pinName);
  }

  public getPinName(pinNumber: number): string {
    return this.pins[pinNumber].getName();
  }

  public setPinState(pinNumber: number, pinState: boolean) {
    this.pins[pinNumber].setState(pinState);
  }

  public getPinState(pinNumber: number): boolean {
    return this.pins[pinNumber].getState();
  }

  public changePinState(pinNumber: number, pinState: boolean) {
    this.pins[pinNumber].changeState();
  }

  public getPins(): Pin[] {
    return this.pins;
  }

}
