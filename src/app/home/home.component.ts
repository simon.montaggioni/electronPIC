import { Component, OnInit } from "@angular/core";
import { Compiler } from "../shared/compiler/compiler";
import * as $ from 'jquery';
@Component({
  selector: "app-home",
  templateUrl: "./home.component.html",
  styleUrls: ["./home.component.css"]
})
export class HomeComponent implements OnInit {
  private static counter = 1;
  public title = "PIC Simulator V0.1";

  public picImage = "pic" + "1" + ".png";
  private items = new Map<string, string>();
  private compiler = new Compiler();
  public outputCode;
  // public fileText;
  code = "";

  constructor() { }

  ngOnInit() {
  }

  public addItem() {
    HomeComponent.counter++;
    this.items.set("key" + HomeComponent.counter, "value " + HomeComponent.counter);
  }

  public getItems() {
    this.addItem();
    this.items.forEach((value: string, key: string) => {
      console.log(key, value);
    });
  }


}

