import { Component, OnInit } from "@angular/core";
import { Microcontroller } from "../shared/devices/microcontroller/microcontroller";
import { isNullOrUndefined } from "util";
import {NgbModal, ModalDismissReasons} from "@ng-bootstrap/ng-bootstrap";
import { Subscription } from "rxjs/Subscription";

@Component({
  selector: "app-simulator",
  templateUrl: "./simulator.component.html",
  styleUrls: ["./simulator.component.css"]
})
export class SimulatorComponent implements OnInit {
  public imageNumber = 1;
  public greenLed = "green_led" + this.imageNumber + ".png";
  public microcontroller = new Microcontroller("16f84A");
  public memoryRegister;
  public registerValue: string;
  public memoryRegisterSet;
  public setMemoryRegisterAddress;
  public getMemoryRegisterAddress;
  public registerValueSet: string;
  public ramValues;
  public flashValues;
  public memoryValues;
  public code;
  public activateRun = false;
  public setRegisterValue; //  = {bit0: "0", bit1: "0", bit2: "0", bit3: "0", bit4: "0", bit5: "0", bit6: "0", bit7: "0"};
  public getRegisterValue; //  = {bit0: "0", bit1: "0", bit2: "0", bit3: "0", bit4: "0", bit5: "0", bit6: "0", bit7: "0"};
  public portA; //  = {bit0: "0", bit1: "0", bit2: "0", bit3: "0", bit4: "0", bit5: "0", bit6: "0", bit7: "0"};
  public portB; //  = {bit0: "1", bit1: "0", bit2: "0", bit3: "0", bit4: "0", bit5: "0", bit6: "0", bit7: "0"};
  public workingRegister;
  public statusRegister;
  public title: string;
  public showRegisters = true;
  public data = new Array();
  public gralPurposeRegisters = new Array();
  public specialPurposeRegisters = new Array();
  public clockSpeed = 0;
  private play: any;
  private pause = true;
  private resetMicro = false;

  public cycleExecutionDetails = {
    microcontrollerModel: "N/A",
    programCounter: "N/A",
    flashAddress: "N/A",
    flashInstruction: "N/A",
    currentInstruction: "N/A",
    nextInstruction: "N/A",
    readRamAddress: "N/A",
    writeRamAddress: "N/A",
    totalCycles: "N/A",
  };
  private sub: Subscription;
  public flashRegisters = new Array();

  ngOnInit() {}

  constructor(private modalService: NgbModal) {
    this.setMemoryRegisterAddress = 0;
    this.getMemoryRegisterAddress = 0;
    // this.setRegisterValue = {bit0: "0", bit1: "0", bit2: "0", bit3: "0", bit4: "0", bit5: "0", bit6: "0", bit7: "0"};
    // this.getRegisterValue = {bit0: "0", bit1: "0", bit2: "0", bit3: "0", bit4: "0", bit5: "0", bit6: "0", bit7: "0"};
    // this.portA = {bit0: "0", bit1: "0", bit2: "0", bit3: "0", bit4: "0", bit5: "0", bit6: "0", bit7: "0"};
    // this.portB = {bit0: "1", bit1: "0", bit2: "0", bit3: "0", bit4: "0", bit5: "0", bit6: "0", bit7: "0"};
    this.setRegisterValue = ["0", "0", "0", "0", "0", "0", "0", "0"];
    this.getRegisterValue = ["0", "0", "0", "0", "0", "0", "0", "0"];
    this.portA = ["0", "0", "0", "0", "0", "x", "x", "x"];
    this.portB = ["0", "0", "0", "0", "0", "0", "0", "0"];
    this.workingRegister = ["0", "0", "0", "0", "0", "0", "0", "0"];
    this.statusRegister = ["0", "0", "0", "0", "0", "0", "0", "0"];
  }

  public showRam() {
    this.ramValues = this.microcontroller.printAllRamValues();
    this.memoryValues = this.ramValues;
  }

  public getRam() {
    this.gralPurposeRegisters = [];
    this.specialPurposeRegisters = [];

    // tslint:disable-next-line:prefer-const
    let ramValues = this.microcontroller.getAllRamValues();
    // console.log(ramValues);
    for ( let address = 0; address < ramValues.length; address++) {
      // tslint:disable-next-line:prefer-const
      let memoryRegister = {address: 0,
        name: "",
        hexValue: "",
        binValue: "",
        bit0: "",
        bit1: "",
        bit2: "",
        bit3: "",
        bit4: "",
        bit5: "",
        bit6: "",
        bit7: ""};
      // tslint:disable-next-line:prefer-const
      let register = ramValues[address];
      memoryRegister.bit0 = register.binValue.charAt(0);
      memoryRegister.bit1 = register.binValue.charAt(1);
      memoryRegister.bit2 = register.binValue.charAt(2);
      memoryRegister.bit3 = register.binValue.charAt(3);
      memoryRegister.bit4 = register.binValue.charAt(4);
      memoryRegister.bit5 = register.binValue.charAt(5);
      memoryRegister.bit6 = register.binValue.charAt(6);
      memoryRegister.bit7 = register.binValue.charAt(7);
      memoryRegister.address = register.address;
      memoryRegister.name = register.name;
      memoryRegister.hexValue = register.hexValue;
      // console.log(memoryRegister);
      this.gralPurposeRegisters.push(memoryRegister);
      if (memoryRegister.name !== "") {
        this.specialPurposeRegisters.push(memoryRegister);
      }
    }
    this.showRegisters = true;
  }

  public showFlash() {
    // this.flashValues = this.microcontroller.getAllFlashValues();
    // this.memoryValues = this.flashValues;
    this.flashRegisters = [];

    // tslint:disable-next-line:prefer-const
    let flashValues = this.microcontroller.getAllFlashValues();
    // console.log(flashValues);
    for ( let address = 0; address < this.microcontroller.getFlashSize(); address++) {
      // console.log(address);
      // flashValues.array.forEach(flashRegister => {
        // tslint:disable-next-line:prefer-const
      let memoryRegister = {address: 0,
        name: "",
        hexValue: "",
        binValue: "",
        bit0: "",
        bit1: "",
        bit2: "",
        bit3: "",
        bit4: "",
        bit5: "",
        bit6: "",
        bit7: "",
        bit8: "",
        bit9: "",
        bit10: "",
        bit11: "",
        bit12: "",
        bit13: "",
        bit14: ""};
      // tslint:disable-next-line:prefer-const
      let register = flashValues[address];
      memoryRegister.bit0 = register.binValue.charAt(13);
      memoryRegister.bit1 = register.binValue.charAt(12);
      memoryRegister.bit2 = register.binValue.charAt(11);
      memoryRegister.bit3 = register.binValue.charAt(10);
      memoryRegister.bit4 = register.binValue.charAt(9);
      memoryRegister.bit5 = register.binValue.charAt(8);
      memoryRegister.bit6 = register.binValue.charAt(7);
      memoryRegister.bit7 = register.binValue.charAt(6);
      memoryRegister.bit8 = register.binValue.charAt(5);
      memoryRegister.bit9 = register.binValue.charAt(4);
      memoryRegister.bit10 = register.binValue.charAt(3);
      memoryRegister.bit11 = register.binValue.charAt(2);
      memoryRegister.bit12 = register.binValue.charAt(1);
      memoryRegister.bit13 = register.binValue.charAt(0);
      // memoryRegister.bit14 = register.binValue.charAt(14);
      memoryRegister.address = address;
      memoryRegister.name = register.name;
      memoryRegister.hexValue = register.hexValue;
      // console.log(memoryRegister);
      this.flashRegisters.push(memoryRegister);
    // });
    }
  }

  public showRamRegister() {
    // tslint:disable-next-line:prefer-const
    let registerValue = this.microcontroller.displayRegisterValue(parseInt(this.getMemoryRegisterAddress, 10), "ram");
    // console.log("########" + registerValue);
      this.getRegisterValue[0] = registerValue.charAt(0);
      this.getRegisterValue[1] = registerValue.charAt(1);
      this.getRegisterValue[2] = registerValue.charAt(2);
      this.getRegisterValue[3] = registerValue.charAt(3);
      this.getRegisterValue[4] = registerValue.charAt(4);
      this.getRegisterValue[5] = registerValue.charAt(5);
      this.getRegisterValue[6] = registerValue.charAt(6);
      this.getRegisterValue[7] = registerValue.charAt(7);
    // console.log(this.getMemoryRegisterAddress);
  }

  public setRamRegister() {
    // console.log(this.memoryRegister);
    const registerArray = [];
    registerArray[0] = this.setRegisterValue[0];
    registerArray[1] = this.setRegisterValue[1];
    registerArray[2] = this.setRegisterValue[2];
    registerArray[3] = this.setRegisterValue[3];
    registerArray[4] = this.setRegisterValue[4];
    registerArray[5] = this.setRegisterValue[5];
    registerArray[6] = this.setRegisterValue[6];
    registerArray[7] = this.setRegisterValue[7];
    const registerValueSet = this.microcontroller.convertArray2Register(registerArray);
    // console.log(registerArray);
    // console.log(this.registerValueSet);
    this.microcontroller.setRamRegisterValue(parseInt(this.setMemoryRegisterAddress, 10), registerValueSet);
    this.getRam();
  }

  public burnFlash() {
    if (!isNullOrUndefined(this.code)) {
      this.microcontroller.burnFlash(this.code);
      this.activateRun = true;
      alert("Flash Memory Burned Successfully");
    } else {
      console.log("You must load the program code");
      alert("You must load the program code");
    }
  }

  public runBySteps() {
    let registerValue: string;
    this.microcontroller.execCycle();
    const cycleExecutionDetails = this.microcontroller.getCycleDetails();
    this.cycleExecutionDetails.microcontrollerModel = cycleExecutionDetails.microcontrollerModel;
    this.cycleExecutionDetails.programCounter = cycleExecutionDetails.programCounter;
    this.cycleExecutionDetails.flashAddress = cycleExecutionDetails.flashAddress;
    this.cycleExecutionDetails.flashInstruction = cycleExecutionDetails.flashInstruction;
    this.cycleExecutionDetails.currentInstruction = cycleExecutionDetails.currentInstruction;
    this.cycleExecutionDetails.nextInstruction = cycleExecutionDetails.nextInstruction;
    this.cycleExecutionDetails.totalCycles = cycleExecutionDetails.totalCycles;
      registerValue = this.microcontroller.displayRegisterValue(5, "ram");
      this.portA[0] = registerValue.charAt(0);
      this.portA[1] = registerValue.charAt(1);
      this.portA[2] = registerValue.charAt(2);
      this.portA[3] = registerValue.charAt(3);
      this.portA[4] = registerValue.charAt(4);
      this.portA[5] = "x";
      this.portA[6] = "x";
      this.portA[7] = "x";

      registerValue = this.microcontroller.displayRegisterValue(6, "ram");
      this.portB[0] = registerValue.charAt(0);
      this.portB[1] = registerValue.charAt(1);
      this.portB[2] = registerValue.charAt(2);
      this.portB[3] = registerValue.charAt(3);
      this.portB[4] = registerValue.charAt(4);
      this.portB[5] = registerValue.charAt(5);
      this.portB[6] = registerValue.charAt(6);
      this.portB[7] = registerValue.charAt(7);

      registerValue = this.microcontroller.getWorkingRegister();
      console.log("ESTO ES LO QUE DEBERÍA MOSTRAR LA VISTA DEL SIMULADOR >>>>>>" + registerValue);
      this.workingRegister[0] = registerValue.charAt(7);
      this.workingRegister[1] = registerValue.charAt(6);
      this.workingRegister[2] = registerValue.charAt(5);
      this.workingRegister[3] = registerValue.charAt(4);
      this.workingRegister[4] = registerValue.charAt(3);
      this.workingRegister[5] = registerValue.charAt(2);
      this.workingRegister[6] = registerValue.charAt(1);
      this.workingRegister[7] = registerValue.charAt(0);

      registerValue = this.microcontroller.getRamRegisterValue(3);
      this.statusRegister[0] = registerValue.charAt(0);
      this.statusRegister[1] = registerValue.charAt(1);
      this.statusRegister[2] = registerValue.charAt(2);
      this.statusRegister[3] = registerValue.charAt(3);
      this.statusRegister[4] = registerValue.charAt(4);
      this.statusRegister[5] = registerValue.charAt(5);
      this.statusRegister[6] = registerValue.charAt(6);
      this.statusRegister[7] = registerValue.charAt(7);

      this.getRam();
  }

  public run() {
      this.play = setInterval( () => {
        if (!this.pause) {
          this.runBySteps();
        }
      }, 1000);
      // for (let i = 0; i < 50; i++) {
      //   this.runBySteps();
      //   this.delay(500);
      // }
  }

  public tooglePause() {
    this.pause = (this.pause) ? false : true;
  }

  public reset() {
    this.resetMicro = true;
    this.microcontroller.reset();
  }


  public fileUpload(event) {
    const reader = new FileReader();
    reader.readAsText(event.srcElement.files[0]);
    const me = this;
    reader.onload = function() {
      me.code = reader.result;
    };
  }

  public activateRegisters() {
    this.showRegisters = true;
  }

  public getColor(registerBit) {
    switch (registerBit) {
      case "0":
        return "#1177EF";
      case "1":
        return "#FE1106";
      default:
        return "gray";
    }
  }
  public setBoxColor(event) {
    switch (event) {
      case "0":
        return "#1177EF";
      case "1":
        return "#FE1106";
      default:
        return "gray";
    }
  }

  public setPortA() {
    const registerArray = [];
    registerArray[0] = this.portA[0];
    registerArray[1] = this.portA[1];
    registerArray[2] = this.portA[2];
    registerArray[3] = this.portA[3];
    registerArray[4] = this.portA[4];
    registerArray[5] = "0";
    registerArray[6] = "0";
    registerArray[7] = "0";
    const registerValueSet = this.microcontroller.convertArray2Register(registerArray);
    this.microcontroller.setRamRegisterValue(5, registerValueSet);
  }

  public setPortB() {
    const registerArray = [];
    registerArray[0] = this.portB[0];
    registerArray[1] = this.portB[1];
    registerArray[2] = this.portB[2];
    registerArray[3] = this.portB[3];
    registerArray[4] = this.portB[4];
    registerArray[5] = this.portB[5];
    registerArray[6] = this.portB[6];
    registerArray[7] = this.portB[7];
    const registerValueSet = this.microcontroller.convertArray2Register(registerArray);
    this.microcontroller.setRamRegisterValue(6, registerValueSet);
  }

  public updateValue(event) {
    // let value = event.target.value;
    this.title = event.path[0].attributes.id.nodeValue;
    console.log(this.title);

    console.log(event.target.value);
    console.log(event.path[0].attributes.id.nodeValue);
  }

  public toggleState(registro, bit) {
    const bitPosition = parseInt(bit, 10);
    switch (registro) {
      case "setRegValue":
            this.setRegisterValue[bitPosition] = (this.setRegisterValue[bitPosition] === "1") ? "0" : "1";
        break;
      case "getRegValue":
            this.getRegisterValue[bitPosition] = (this.getRegisterValue[bitPosition] === "1") ? "0" : "1";
        break;
      case "portA":
            this.portA[bitPosition] = (this.portA[bitPosition] === "1") ? "0" : "1";
            this.setPortA();
        break;
      case "portB":
            this.portB[bitPosition] = (this.portB[bitPosition] === "1") ? "0" : "1";
            this.setPortB();
        break;

      default:
        break;
    }
    // console.log(bit);
  }

  openVerticallyCentered(content) {
    this.modalService.open(content, { centered: true });
  }

  public delay(ms: number) {
    return new Promise( resolve => setTimeout(resolve, ms) );
  }


}
