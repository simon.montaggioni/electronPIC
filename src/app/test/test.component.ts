import { Component, OnInit } from "@angular/core";
import { Device } from "../shared/devices/device";

@Component({
  selector: "app-test",
  templateUrl: "./test.component.html",
  styleUrls: ["./test.component.css"]
})
export class TestComponent implements OnInit {
  public droppedItems = [];
  public connections = [];
  public items: Device[];
  public connectionsTable: string[];
  public runSimulationFlag = false;
  public variable = "";
  public cambio = "";

  onItemDrop(e: any) {
    // Get the dropped data here
    this.droppedItems.push(e.dragData);
    console.log("some: " + typeof(e) + " dropped");
  }

  constructor() {
    this.items = [];
    this.connectionsTable = [];
    this.loadDevices();
  }

  ngOnInit() {
    for ( let i = 0; i < 3; i++) {
      const button = { name: i, pin: true };
      this.connections.push( button );
    }
    this.variable = this.cambio;
  }

  public loadDevices() {
    this.items.push(new Device("led", 1));
    this.items.push(new Device("motor", 2));
    this.items.push(new Device("button", 1));
  }

  public changeState (i: any) {
    this.connections[i].pin = (this.connections[i].pin) ? false : true;
    console.log(`button ${i} state ${this.connections[i].pin}`);
    this.runSimulation();
  }

  public setConn(itemPosition: number, connectionPin: number,  pinNumber: number, pinState: boolean) {
    console.log("item: " + itemPosition);
    console.log("numero de pin: " + pinNumber);
    console.log("connection pin: " + connectionPin);
    console.log("item pin state: " + pinState);
    // this.droppedItems[itemPosition].setPinState(pinNumber, this.connections[connectionPin].pin);
    console.log(`estado prueba ${this.droppedItems[itemPosition].getName()} ${this.droppedItems[itemPosition].getPinState(pinNumber)}`);
    const connection = itemPosition + "-" + pinNumber + "-" + connectionPin;
    this.connectionsTable.push(connection);
  }

  public runSimulation() {
    let connectionIndex = 0;
    for (const connection of this.connectionsTable) {
      console.log(connection);
      const values = connection.split("-");
      const itemPosition = values[0];
      const pinNumber = values[1];
      const connectionPin = values[2];
      console.log(`connectionIndex [${connectionIndex}] item position ${itemPosition} pin [${pinNumber}] state before [${this.droppedItems[itemPosition].getPinState(pinNumber)}]`);
      this.droppedItems[itemPosition].setPinState(pinNumber, this.connections[connectionPin].pin);
      console.log(`connectionIndex [${connectionIndex}] item position ${itemPosition} pin [${pinNumber}] state  after [${this.droppedItems[itemPosition].getPinState(pinNumber)}]`);
      connectionIndex++;
    }
  }

}


