import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";
import { NgbModule } from "@ng-bootstrap/ng-bootstrap";
import { NgDragDropModule } from "ng-drag-drop";
import { Routes, RouterModule } from "@angular/router";
import { AppComponent } from "./app.component";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { DragulaModule } from "ng2-dragula/ng2-dragula";
import { DndModule } from "ng2-dnd";
// import { phaser } from "phaser-ce/typescript/phaser";
import { HomeComponent } from "./home/home.component";
import { HelpComponent } from "./help/help.component";
import { TextEditorComponent } from "./textEditor/textEditor.component";
import { SimulatorComponent } from "./simulator/simulator.component";
import { TestComponent } from "./test/test.component";

import { ToBinaryService } from "./services/toBinary.service";
import { TabComponent } from "./tab/tab.component";
const appRoutes: Routes = [
  {path: "", component: HomeComponent},
  {path: "home", component: HomeComponent},
  {path: "editor", component: TextEditorComponent},
  {path: "simulator", component: SimulatorComponent},
  {path: "help", component: HelpComponent},
  { path: "test", component: TestComponent }
];

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    TextEditorComponent,
    SimulatorComponent,
    TestComponent,
    TabComponent,
    HelpComponent
  ],
  imports: [
    NgbModule.forRoot(),
    NgDragDropModule.forRoot(),
    RouterModule.forRoot( appRoutes ),
    FormsModule,
    DragulaModule,
    DndModule.forRoot(),
    // phaser,
    BrowserAnimationsModule,
    BrowserModule
  ],
  providers: [ToBinaryService],
  bootstrap: [AppComponent]
})
export class AppModule { }
