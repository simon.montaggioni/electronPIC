import { Component, OnInit, NgModule } from "@angular/core";
import { Compiler } from "../shared/compiler/compiler";
import { Angular2Txt } from "angular2-txt";
import {NgbModule} from "../../../node_modules/@ng-bootstrap/ng-bootstrap";
import {NgbModal, ModalDismissReasons} from "@ng-bootstrap/ng-bootstrap";
@NgModule({
  imports: [NgbModule]
})
@Component({
  selector: "app-texteditor",
  templateUrl: "./textEditor.component.html",
  styleUrls: ["./textEditor.component.css"]
})
export class TextEditorComponent implements OnInit {
  private static counter = 1;
  public title = "PIC Simulator V0.1";
  private items = new Map<string, string>();
  private compiler = new Compiler();
  public outputCode;

  // public fileText;
  code = "";
  closeResult: string;

  constructor(private modalService: NgbModal) {}

  ngOnInit() {}

  public name(value) {
    this.title = value;
  }
  public addItem() {
    TextEditorComponent.counter++;
    this.items.set(
      "key" + TextEditorComponent.counter,
      "value " + TextEditorComponent.counter
    );
  }

  public getItems() {
    this.addItem();
    this.items.forEach((value: string, key: string) => {
      console.log(key, value);
    });
  }

  public runCompiler() {
    this.compiler.run(this.code);
    this.outputCode = this.compiler.getInstructionsMap();
    const data = [
      {
        out: this.outputCode
      }
    ];
    const today = new Date();
    const dd = today.getDate();
    const mm = today.getMonth() + 1; // hoy es 0!
    const yyyy = today.getFullYear();
    // tslint:disable-next-line:no-unused-expression
    new Angular2Txt(data, "out_" + dd + "_" + mm + "_" + yyyy, ".sim");
  }

  public dowloandarchive() {
    this.outputCode = this.code;
    const data = [
      {
        out: this.outputCode
      }
    ];
    const today = new Date();
    const dd = today.getDate();
    const mm = today.getMonth() + 1; // hoy es 0!
    const yyyy = today.getFullYear();
    // tslint:disable-next-line:no-unused-expression
    new Angular2Txt(data, "out_code_" + dd + "_" + mm + "_" + yyyy, ".asm");

  }

  public getRealCode() {
    this.outputCode = this.compiler.getRealCode(); // .replace(/\n/gi, "<br>");
      // this.modalService.open(, { centered: true });
      this.title = "Get Real Code";
  }

  public getOutputCode() {
    this.outputCode = this.compiler.getOutputCode();
    // this.modalService.open(, { centered: true });
    this.title = "Get Output Code";
  }

  public getEquTable() {
    this.outputCode = this.compiler.getEquTable();
    // this.modalService.open(, { centered: true });
    this.title = "Get Equtable";
  }

  public getInstructions() {
    this.outputCode = this.compiler.getInstructions();
    // this.modalService.open(, { centered: true });
    this.title = "Get Instructions";
  }

  public getHexInstructions() {
    this.outputCode = this.compiler.getHexInstructions();
    // this.modalService.open(, { centered: true });
    this.title = "Get HexInstructions";
  }

  public getBinaryCode() {
    this.outputCode = this.compiler.getBinaryCode();
    // this.modalService.open(, { centered: true });
    this.title = "Get Binary Code";
  }

  fileUpload(event) {
    console.log(event);
    const reader = new FileReader();
    reader.readAsText(event.srcElement.files[0]);
    const me = this;
    reader.onload = function() {
      me.code = reader.result;
      console.log(reader.result);
    };
  }
  openVerticallyCentered(content) {
    this.modalService.open(content, { centered: true });
  }

}

