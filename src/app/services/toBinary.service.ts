import { Injectable } from "@angular/core";
import { isNullOrUndefined } from "util";

@Injectable()
export class ToBinaryService {

  public toBinaryNumber (register: string): string {
    // console.log("toBinaryNumber: register ->" + register);
    let result = "";
    if (this.isHex(register)) {
      // console.log("ishex");
      result = this.hex2Binary(register);
    } else if (this.isDecimal(register)) {
      // console.log("isDec");
      result = this.decimal2Binary(register);
    } else if (this.isBinary(register)) {
      // console.log("isBin");
      result = this.binary2Binary(register);
    } else {
    }
    // console.log("toBinaryNumber out: result ->" + result);
    return result;
  }

  public isHex (number: string): boolean {
    let result = false;
    if (!isNullOrUndefined(number)) {
      if (number.search(/H|X/) !== -1) {
        result = true;
      }
    }
    return result;
  }

  public isBinary ( number: string):  boolean {
    let result = false;
    if (!isNullOrUndefined(number)) {
      if (number.search(/B/) !== -1) {
        result = true;
      }
    }
    return result;
  }

  public isDecimal (number: string): boolean {
    let result = false;
    if (!isNullOrUndefined(number)) {
      if ((number.search(/D|\./) !== -1) || (Number.isInteger(parseInt(number, 0)))) {
        result = true;
      }
    }
    return result;
  }

  public hex2Binary (number: string): string {
    const hex = number.replace(/\'|\"/g, "").replace(/H/ig, "0x");
    const result = parseInt(hex, 0).toString(2);
    return result;
  }

  public decimal2Binary (number: string): string {
    const decimal = number.replace(/\'|\"/g, "").replace(/\.|d/ig, "");
    // console.log("decimal: {" + decimal + "}");
    const result = parseInt(decimal, 0).toString(2);
    // console.log("d2b: " + result);
    return result;
  }

  public binary2Binary (number: string): string {
    const decimal = number.replace(/\'|\"/g, "").replace(/\.|b/ig, "").slice(number.length - 8, number.length);
    // console.log("decimal: {" + decimal + "}");
    const result = decimal;  // parseInt(decimal, 0).toString(2);
    // console.log("b2b: " + result);
    return result;
  }

  public toHex(number: number): string {
    return "0X" + parseInt(number + "", 0).toString(16).toUpperCase();
  }

  public fillLeftZeros (number: string, zerosQuantity: number): string {
    if (number.length < zerosQuantity) {
      for ( let len = number.length; number.length < zerosQuantity; len++) {
        number = "0" + number;
        // console.log("binary number: ->> {" + number + "}");
      }
    }
    return number;
  }
}
